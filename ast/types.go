package ast

type IntType struct {
}

func (t *IntType) dynamicTypes(visited map[interface{}]bool) map[string]DynamicType {
	return map[string]DynamicType{}
}

func (t *IntType) DynamicTypes() map[string]DynamicType {
	return t.dynamicTypes(map[interface{}]bool{})
}

type FloatType struct {
}

func (t *FloatType) dynamicTypes(visited map[interface{}]bool) map[string]DynamicType {
	return map[string]DynamicType{}
}

func (t *FloatType) DynamicTypes() map[string]DynamicType {
	return t.dynamicTypes(map[interface{}]bool{})
}

type StringType struct {
}

func (t *StringType) dynamicTypes(visited map[interface{}]bool) map[string]DynamicType {
	return map[string]DynamicType{}
}

func (t *StringType) DynamicTypes() map[string]DynamicType {
	return t.dynamicTypes(map[interface{}]bool{})
}

type TimeType struct {
}

func (t *TimeType) dynamicTypes(visited map[interface{}]bool) map[string]DynamicType {
	return map[string]DynamicType{
		"datetime": {
			JSON:       "string",
			JavaScript: "Date",
			Go:         "time.Time",
		},
	}
}

func (t *TimeType) DynamicTypes() map[string]DynamicType {
	return t.dynamicTypes(map[interface{}]bool{})
}

type BoolType struct {
}

func (t *BoolType) dynamicTypes(visited map[interface{}]bool) map[string]DynamicType {
	return map[string]DynamicType{}
}

func (t *BoolType) DynamicTypes() map[string]DynamicType {
	return t.dynamicTypes(map[interface{}]bool{})
}

type AnyType struct {
}

func (t *AnyType) dynamicTypes(visited map[interface{}]bool) map[string]DynamicType {
	return map[string]DynamicType{}
}

func (t *AnyType) DynamicTypes() map[string]DynamicType {
	return t.dynamicTypes(map[interface{}]bool{})
}

type NullableType struct {
	Type Type
}

func (t *NullableType) dynamicTypes(visited map[interface{}]bool) map[string]DynamicType {
	return t.Type.dynamicTypes(visited)
}

func (t *NullableType) DynamicTypes() map[string]DynamicType {
	return t.dynamicTypes(map[interface{}]bool{})
}

type CustomType struct {
	Name string
	Type Type
}

func (t *CustomType) dynamicTypes(visited map[interface{}]bool) map[string]DynamicType {
	if visited[t] {
		return map[string]DynamicType{}
	}
	visited[t] = true

	return t.Type.dynamicTypes(visited)
}

func (t *CustomType) DynamicTypes() map[string]DynamicType {
	return t.dynamicTypes(map[interface{}]bool{})
}

type ObjectType struct {
	Fields []*NamedType
}

func (t *ObjectType) dynamicTypes(visited map[interface{}]bool) map[string]DynamicType {
	if visited[t] {
		return map[string]DynamicType{}
	}
	visited[t] = true

	m := map[string]DynamicType{}
	for _, f := range t.Fields {
		for k, v := range f.Type.dynamicTypes(visited) {
			m[k] = v
		}
	}
	return m
}

func (t *ObjectType) DynamicTypes() map[string]DynamicType {
	return t.dynamicTypes(map[interface{}]bool{})
}

type ArrayType struct {
	Type Type
}

func (t *ArrayType) dynamicTypes(visited map[interface{}]bool) map[string]DynamicType {
	if visited[t] {
		return map[string]DynamicType{}
	}
	visited[t] = true
	return t.Type.dynamicTypes(visited)
}

func (t *ArrayType) DynamicTypes() map[string]DynamicType {
	return t.dynamicTypes(map[interface{}]bool{})
}

type MapType struct {
	Type Type
}

func (t *MapType) dynamicTypes(visited map[interface{}]bool) map[string]DynamicType {
	if visited[t] {
		return map[string]DynamicType{}
	}
	visited[t] = true
	return t.Type.dynamicTypes(visited)
}

func (t *MapType) DynamicTypes() map[string]DynamicType {
	return t.dynamicTypes(map[interface{}]bool{})
}

type EnumType struct {
	Values []EnumValue
}

func (ed *EnumType) dynamicTypes(visited map[interface{}]bool) map[string]DynamicType {
	return map[string]DynamicType{}
}

func (ed *EnumType) DynamicTypes() map[string]DynamicType {
	return ed.dynamicTypes(map[interface{}]bool{})
}

type EnumValue struct {
	Name  string
	Value string
}
