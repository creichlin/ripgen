package ast

type AST struct {
	Name             string
	Calls            []*Call
	TypeDeclarations []*TypeDeclaration
}

type Type interface {
	DynamicTypes() map[string]DynamicType
	dynamicTypes(visited map[interface{}]bool) map[string]DynamicType
}

type DynamicType struct {
	JSON       string
	JavaScript string
	Go         string
}

type TypeDeclaration struct {
	Name string
	Type Type
}

func (a *AST) GetTypeDeclaration(name string) *TypeDeclaration {
	for _, td := range a.TypeDeclarations {
		if td.Name == name {
			return td
		}
	}
	return nil
}

func (a *AST) DynamicTypes() map[string]DynamicType {
	return a.dynamicTypes(map[interface{}]bool{})
}

func (a *AST) dynamicTypes(visited map[interface{}]bool) map[string]DynamicType {
	m := map[string]DynamicType{}
	for _, c := range a.Calls {
		if c.Body != nil {
			for k, v := range c.Body.dynamicTypes(visited) {
				m[k] = v
			}
		}
		if t, is := c.Response.(*JSONIO); is {
			for k, v := range t.Type.dynamicTypes(visited) {
				m[k] = v
			}
		}
		for _, p := range c.Parameters {
			for k, v := range p.Type.dynamicTypes(visited) {
				m[k] = v
			}
		}
	}
	return m
}

type Call struct {
	Method     string
	Name       string
	URL        []URLPart
	Parameters []*NamedType
	Body       Type
	Response   IO
}

type IO interface {
}

type JSONIO struct {
	Type Type
}

type BinaryIO struct {
	MimeType string
}

type StreamIO struct {
	MimeType string
}

// PathParameters returns parameters in path
// as a list of named types.
func (c *Call) PathParameters() []*NamedType {
	ts := []*NamedType{}
	for _, pp := range c.URL {
		t, is := pp.(*ParameterURLPart)
		if is {
			ts = append(ts, t.Parameter)
		}
	}
	return ts
}

func (c *Call) AllParameters() []*NamedType {
	return append(c.PathParameters(), c.Parameters...)
}

type URLPart interface{}

type StaticURLPart string

type ParameterURLPart struct {
	Parameter *NamedType
}

type NamedType struct {
	Name string
	Type Type
}
