import go

main visitor:
ast.AST type:
  package ${cpkg}

  import (
    "encoding/json"
    "fmt"
    "log"
    "net/http"
    "strconv"
    "${ipkgImport}"
  )

  type requestFactory func(method, url string, body io.Reader) (*http.Request, error)

  type client struct {
    c          *http.Client
    baseURL    string
    newRequest requestFactory
  }

  func Create(baseURL string, c *http.Client, newRequest requestFactory) ${ipkg}.Calls {
    ${"if"} c == nil {
      c = &http.Client{Timeout: time.Second * 5}
    }
    return &client{
      c:          c,
      baseURL:    baseURL,
      newRequest: newRequest,
    }
  }

  {for call in .Calls-}
    @{call}
  {end}

ast.Call type:
  func(ø *client)${.Name | AsID} (
    {-for p in .AllParameters -}
      @{p},
    {-end}
    {-if .Body-}bodyø @{go.type: .Body pkg=ipkg}, {end-}
    @{go.responseIn: .Response}) (@{go.responseOut: .Response name=.Name, pkg=ipkg} error) {
      urlø := @{url:.}

      {if .Body}
        bbø, errø := json.Marshal(bodyø)
        if errø != nil {
          return @{errResponse: .Response name=.Name}
        }
        bodyReaderø := bytes.NewReader(bbø)
      {end}

      reqø, errø := ø.newRequest("${.Method|upper}", ø.baseURL + urlø, {if .Body}bodyReaderø{else}nil{end})
      if errø != nil {
        return @{errResponse: .Response name=.Name}
      }

      @{assignResponse: .Response} ø.c.Do(reqø)
      if errø != nil {
        return @{errResponse: .Response name=.Name}
      }

      @{parseResponse: .Response name=.Name}

      return @{returnResponse: .Response} nil
    }

ast.NamedType type:
  ${.Name | asID} @{go.type: .Type pkg=ipkg}

parseResponse visitor:
ast.JSONIO type:
  binø, errø := ioutil.ReadAll(responseø.Body)
  if errø != nil {
    return @{errResponse:. name=name}
  }
  resultø := @{emptyResponse: .}
  errø = json.Unmarshal(binø, &resultø)
  if errø != nil {
    return @{errResponse:. name=name}
  }
ast.BinaryIO type:
  resultø, errø := ioutil.ReadAll(responseø.Body)
  if errø != nil {
    return @{errResponse:. name=name}
  }
ast.StreamIO type:
  _, errø = io.Copy(streamø, responseø.Body)
  if errø != nil {
    return @{errResponse:. name=name}
  }
<nil> type:


returnResponse visitor:
ast.JSONIO type:
  resultø,
ast.BinaryIO type:
  resultø,
ast.StreamIO type:
<nil> type:

assignResponse visitor:
ast.JSONIO type:
  responseø, errø :=
ast.BinaryIO type:
  responseø, errø :=
ast.StreamIO type:
  responseø, errø :=
* type:
  _, errø =


emptyResponse visitor:
ast.JSONIO type:
  @{go.zeroType: .Type pkg=ipkg}
ast.BinaryIO type:
  []byte{}
* type:

errResponse visitor:
ast.JSONIO type:
  @{emptyResponse:.}, errø
ast.BinaryIO type:
  @{emptyResponse:.}, errø
* type:
  errø

encode visitor:
ast.StringType type:
  url.PathEscape(${val|asID})
ast.TimeType type:
  ${val|asID}.Format(time.RFC3339)
* type:
  ${val|asID}

pathParam visitor:
ast.ParameterURLPart type:
  @{encode:.Parameter.Type val=.Parameter.Name},
* type:

queryParam visitor:
ast.NamedType type:
  @{encode:.Type val=.Name},

url visitor:
ast.Call type:
  fmt.Sprintf("{for up in .URL-}
    @{up}
  {-end}?@{.Parameters}",
    {for up in .URL}@{pathParam:up}{end}
    {for qp in .Parameters}@{queryParam:qp}{end}
  )
ast.StaticURLPart type:
  /${.}
ast.ParameterURLPart type:
  /%v
[]ast.NamedType type:
  {for i, nt, first in .}{if !first}&{end}${nt.Name|asID}=%v{end}
