import ts

main visitor:
ast.AST type:
  import * as types from './types.gen'

  function mapObject<FROM, TO>(
    m: { [key: string]: FROM },
    fn: (this: void, v: FROM) => TO
  ): { [key: string]: TO } {
    const ob: { [key: string]: TO } = {};
    Object.entries(m).forEach((item) => {
      ob[item[0]] = fn(item[1]);
    });
    return ob;
  }

  type fetcher = (url: string, method: ('GET' | 'POST' | 'PUT' | 'DELETE'), body?: undefined | string) => Promise<any>

  export class Client@{ts.genericMapping: .DynamicTypes} {
    private baseUrl: string
    private fetch: fetcher

    {for name, type in .DynamicTypes-}
    public read${name|AsId}: (s: JSON_${name|ASID}) => JS_${name|ASID}
    public write${name|AsId}: (d: JS_${name|ASID}) => JSON_${name|ASID}
    {end}

    constructor (baseUrl: string, fetch: fetcher,
      {for name, type in .DynamicTypes-}
      read${name|AsId}: (s: JSON_${name|ASID}) => JS_${name|ASID},
      write${name|AsId}: (d: JS_${name|ASID}) => JSON_${name|ASID},
      {end}
    ) {
      this.baseUrl = baseUrl
      this.fetch = fetch
      {for name, type in .DynamicTypes-}
      this.read${name|AsId} = read${name|AsId}
      this.write${name|AsId} = write${name|AsId}
      {end}
    }

    {for call in .Calls-}
    {indent}@{call}{end}

    {end}
  }

ast.Call type:
  async ${.Name | asId} (
    {-for p in .AllParameters}@{p}, {end}
    {-if .Body-}
      body: @{ts.type: .Body ns="types", generic="JavaScript", declaration=true},
    {-end-}): Promise<@{returnDeclaration: .Response}> {
    const url = `${"$"}{this.baseUrl}@{url: .}
    {- if .Parameters -}
      ?@{url:.Parameters}
    {- end}`

    {-if .Body}
    const wireBody = @{toWire: .Body s="body"}
    {-end}

    @{responseAssignment: .Response} await this.fetch(url, '${.Method | ASID}'{-if .Body}, JSON.stringify(wireBody){end})

    {indent}@{return: .Response}{end}
  }

ast.NamedType type:
  ${.Name | asId}: @{ts.type: .Type ns="types", generic="JavaScript", declaration=true}

url visitor:
ast.Call type:
  {for up in .URL-}
    @{up}
  {-end}
ast.StaticURLPart type:
  /${.}
ast.ParameterURLPart type:
  /${"$"}{@{encode: .Parameter.Type val=.Parameter.Name | asId}}
[]ast.NamedType type:
  {for i, p, first in .}{if !first}&{end}@{p}{end}
ast.NamedType type:
  ${.Name | asId}=${"$"}{@{encode: .Type val=.Name | asId}}

encode visitor:
ast.StringType type:
  encodeURIComponent(${val})
ast.TimeType type:
  this.writeDatetime(${val})
* type:
  ${val}


responseAssignment visitor:
ast.JSONIO type:
  const resp =
ast.StreamIO type:
  const resp =
ast.BinaryIO type:
  const resp =
<nil> type:


returnDeclaration visitor:
ast.JSONIO type:
  @{ts.type: .Type ns="types", generic="JavaScript", declaration=true}
ast.BinaryIO type:
  ArrayBuffer
ast.StreamIO type:
  ArrayBuffer
<nil> type:
  void

return visitor:
ast.JSONIO type:
  const raw: @{ts.type: .Type ns="types", generic="JSON", declaration="true"} = JSON.parse(await resp.text())
  return @{convert: .Type s = "raw"}
ast.BinaryIO type:
  return resp.arrayBuffer()
ast.StreamIO type:
  return resp.arrayBuffer()
<nil> type:


toWire visitor:
ast.ObjectType type(s = "default"):
  {
    {for f in .Fields-}
      ${f.Name | asId}: {indent}@{f.Type s = s + "." + (f.Name | asId)}{end},
    {end-}
  }
ast.CustomType type:
  @{.Type s = s}
ast.NullableType type:
  (${s} === null) ? null : @{.Type s = s}
ast.ArrayType type:
  ${s}.map((e: @{ts.type: .Type ns="types", generic="JavaScript", declaration=true}) => {
    return @{.Type s="e"}
  })
ast.MapType type:
  mapObject(${s}, (e: @{ts.type: .Type ns="types", generic="JavaScript", declaration=true}) => {
    return @{.Type s="e"}
  })
ast.TimeType type:
  this.writeDatetime(${s})
* type:
  ${s}


convert visitor:
ast.ObjectType type(s = "default"):
  {
    {for f in .Fields-}
      ${f.Name | asId}: {indent}@{f.Type s = s + "." + (f.Name | asId)}{end},
    {end-}
  }
ast.CustomType type:
  @{.Type s = s}
ast.NullableType type:
  (${s} === null) ? null : @{.Type s = s}
ast.ArrayType type:
  ${s}.map((e: @{ts.type: .Type ns="types", generic="JSON", declaration=true}) => {
    return @{.Type s="e"}
  })
ast.MapType type:
  mapObject(${s}, (e: @{ts.type: .Type ns="types", generic="JSON", declaration=true}) => {
    return @{.Type s="e"}
  })
ast.TimeType type:
  this.readDatetime(${s})
* type:
  ${s}