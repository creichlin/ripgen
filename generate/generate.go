package generate

import (
	"io/ioutil"
	"path/filepath"
	"runtime"
	"strings"

	"gitlab.com/akabio/fmtid"
	"gitlab.com/akabio/iotool"
	"gitlab.com/creichlin/gogen"
	"gitlab.com/creichlin/gogen/parser"
	"gitlab.com/creichlin/ripgen/ast"
	"golang.org/x/tools/imports"
)

func Generate(ast *ast.AST, destFile, template string, vars map[string]string) error {
	_, filename, _, _ := runtime.Caller(0)
	dir := filepath.Dir(filename)
	data, err := ioutil.ReadFile(filepath.Join(dir, template))
	if err != nil {
		return err
	}

	templateDir := filepath.Dir(filepath.Join(dir, template))

	gg, err := parser.Parse(string(data), template, includeFrom(templateDir))
	if err != nil {
		return err
	}

	// d, _ := yaml.Marshal(gg)
	// fmt.Println(string(d))

	opts := []gogen.Option{}
	for k, v := range fmtid.ASCII.Formats {
		opts = append(opts, gogen.FilterOption(k, v))
	}

	for v, val := range vars {
		opts = append(opts, gogen.WithVar(v, val))
	}

	result, err := gogen.Execute(gg, ast, opts...)
	if err != nil {
		return err
	}

	if strings.HasSuffix(destFile, ".go") {
		result, err = goImports(destFile, result)
		if err != nil {
			return err
		}
	}
	return iotool.WriteFileIfChanged(destFile, []byte(result), 0600)
}

func includeFrom(dir string) func(string) (string, string, error) {
	return func(n string) (string, string, error) {
		source := filepath.Join(dir, n+".gg")
		d, err := ioutil.ReadFile(source)
		if err != nil {
			return "", "", err
		}
		return string(d), source, nil
	}
}

// goImports takes the source and formats it and updates imports
func goImports(file, source string) (string, error) {
	result, err := imports.Process(file, []byte(source), &imports.Options{Comments: true})
	if err != nil {
		return "//  goimports failed, there are errors!\n" + source, nil
	}
	return string(result), nil
}
