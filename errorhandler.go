package ripgen

import (
	"errors"
	"fmt"
	"log"
	"net/http"
)

var DefaultErrorHandler = ConsoleLogErrorHandler(RipErrorHandler(CatchAllErrorHandler()))

// RipErrorHandler handles all errors produced by ripgen and forwards other errors to the next
// error handler
func RipErrorHandler(next func(r *http.Request, err error) (int, []byte)) func(r *http.Request, err error) (int, []byte) {
	return func(r *http.Request, err error) (int, []byte) {
		var badRequest BadRequestError
		if errors.As(err, &badRequest) {
			return http.StatusBadRequest, []byte(err.Error())
		}

		return next(r, err)
	}
}

// CatchAllErrorHandler returns for all errors an internal server error
func CatchAllErrorHandler() func(r *http.Request, err error) (int, []byte) {
	return func(r *http.Request, err error) (int, []byte) {
		return http.StatusInternalServerError, []byte(fmt.Sprintf("internal server error, %v", err))
	}
}

// ConsoleLogErrorHandler forwards all error handling to next and prints the resulting errors to the console
// using log statements
func ConsoleLogErrorHandler(next func(r *http.Request, err error) (int, []byte)) func(r *http.Request, err error) (int, []byte) {
	return func(r *http.Request, err error) (int, []byte) {
		code, msg := next(r, err)
		log.Printf("error %v: %v", code, string(msg))
		return code, msg
	}
}
