# RIPGen

Not maintained, see https://gitlab.com/akabio/ripgen

## Declaration

The declaration consists of a list of request definitions and opionally data types.

### Requests

    get /enum {
      name: getEnum
      body: string
      response: string
    }

The first part is a HTTP verb `get`, `post`, `put` or `delete`. It is followed by the path.
The request declaration body has a mandatory `name:` directive which declares the name of the method that will handle this request.
The directives `body:` and `response:` are optional and can define a data type.
Instead a response: a `binaryResponse:` or `streamResponse:` directive can be used.

### Path

The path must start with a / and can contain variables like:

    /foo/{bar string}

Additionaly it can define parameters:

    /foo?{q string}&{n int}

They will add parameters to the request handler method like

    Request(q string, n int) error

### Body/response

Body and response can be complex types, they will be added as input/output parameters to the methoid signature when defined like:

    Request(body *MyBody) (MyResponse, error)

### DataTypes

#### Scalars

Scalars in the meaning of non compound types are:

- int
- float
- time
- bool
- enum
- string

The enum type has it's possible values defined in comma separated pairs of `name "value"`:

    `enum (default "", foo, bar, none "n")`

If no string value is given it's string representation equals to the name.

#### Arrays

Arrays are defined by surounding it's element type by square brackets:

    [string] // an array if strings

#### Maps

Maps keys are always of string type. They are defined by surounding it's value type by curly braces:

    {int} // a map of string->int

#### Objects

Objects have multiple named fields where each one can have it's own type. They are also defined using curly braces but they have multiple field name -> value pairs:

    {
      count  int
      name   string
      active bool
    }

Field names must always be lower case.

#### Nullables

Data types can be nullable, meaning that their possible values ad well as null or undefined is a valid state. The default is non nullable and nullability canbe marked by appending a questionmark.

    [string?]? // is an array of strings that can be null and can contain null values

Usually it's best to only allow null values when there is a good reason to do so.

#### Type declarations

Types can be used directly inside request declarations as well as beeing defined outside and then refrenced by it's name:

    type Foo { name string }

    get /foos {
      response: Foo
    }

Custom types must always be Uppercase. Onject types must always be at the top level, either directly under response:/body: directives or types. If objects need to be nested eacdh one has to by defined using a type.
