package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/creichlin/ripgen"
	"gitlab.com/creichlin/ripgen/test/example/calls"
	"gitlab.com/creichlin/ripgen/test/example/domain"
	"gitlab.com/creichlin/ripgen/test/example/hprof"
	"gitlab.com/creichlin/ripgen/test/example/httph"
)

func callsF(w http.ResponseWriter, r *http.Request, p httprouter.Params) (calls.Calls, error) {
	return hprof.Profile(&domain.Domain{}, func(i hprof.Invocation) {
		fmt.Println(i)
	}), nil
}

func main() {
	router := httph.CreateRouter(callsF, ripgen.DefaultErrorHandler)

	log.Fatal(http.ListenAndServe(":8001", router))
}
