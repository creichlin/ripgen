package main

import (
	"testing"

	"gitlab.com/creichlin/ripgen"
	"gitlab.com/creichlin/ripgen/test/example/httph"
	"gitlab.com/creichlin/ripgen/test/internal/test"
)

func TestExample(t *testing.T) {
	tc := test.New(t, httph.CreateRouter(callsF, ripgen.DefaultErrorHandler))
	defer tc.Close()
	resp := tc.Get("/foo/5?count=10&order=bla", map[string]interface{}{})
	resp.Expect.Status.ToBe(200)
	resp.Expect.BodyStr.ToBe("[1,2,3]")
}
