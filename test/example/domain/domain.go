package domain

import (
	"fmt"

	"gitlab.com/creichlin/ripgen/test/example/calls"
)

type Domain struct {
}

func (d *Domain) CreateFoo(body calls.CreateFooBody) error {
	fmt.Println("create foo")
	return nil
}

func (d *Domain) GetFooByID(id int, count int, order string, body calls.GetFooByIDBody) ([]int, error) {
	fmt.Printf("get foo by id %v\n", id)
	return []int{1, 2, 3}, nil
}

func (d *Domain) Trigger() error {
	return nil
}

func (d *Domain) Global() (int, error) {
	return 2, nil
}

func (d *Domain) MapPointers() (calls.MapPointersResponse, error) {
	return calls.MapPointersResponse{}, nil
}
