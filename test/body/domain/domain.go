package domain

import "gitlab.com/creichlin/ripgen/test/body/calls"

type Domain struct {
}

func (d *Domain) GetInt(body int) (int, error) {
	return body, nil
}

func (d *Domain) GetIntList(body []int) ([]int, error) {
	return body, nil
}

func (d *Domain) GetObject(body calls.GetObjectBody) (calls.GetObjectResponse, error) {
	resp := calls.GetObjectResponse(body)
	return resp, nil
}

func (d *Domain) GetObjectDef(body calls.Obj) (calls.Obj, error) {
	return body, nil
}

func (d *Domain) GetParentDef(body calls.Parent) (calls.Parent, error) {
	return body, nil
}

func (d *Domain) GetEnum(body calls.Enum1) (calls.Enum1, error) {
	return body, nil
}

func (d *Domain) GetNullable(body calls.Nullable) (calls.Nullable, error) {
	return body, nil
}

func (d *Domain) GetNullableBodyResponse(body *calls.Obj) (*calls.Obj, error) {
	return body, nil
}

func (d *Domain) PutMap(body map[string]calls.Obj) (map[string]calls.Obj, error) {
	return body, nil
}

func (d *Domain) PutTheDate(body calls.ObjWithDate) error {
	return nil
}

func (d *Domain) PutObjDate(body calls.PutObjDateBody) (calls.PutObjDateResponse, error) {
	return calls.PutObjDateResponse(body), nil
}

func (d *Domain) PutMapDate(body map[string]calls.ObjWithDate) (map[string]calls.ObjWithDate, error) {
	return body, nil
}

func (d *Domain) PutArrayDate(body []calls.ObjWithDate) ([]calls.ObjWithDate, error) {
	return body, nil
}
