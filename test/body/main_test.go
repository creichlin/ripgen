package main

import (
	"net/http"
	"testing"

	"gitlab.com/creichlin/ripgen"
	"gitlab.com/creichlin/ripgen/test/body/calls"
	"gitlab.com/creichlin/ripgen/test/body/httph"
	"gitlab.com/creichlin/ripgen/test/internal/test"
)

func TC(t *testing.T) *test.TC {
	router := httph.CreateRouter(callsF, ripgen.DefaultErrorHandler)
	return test.New(t, router)
}

func TestIntBody(t *testing.T) {
	tc := TC(t)
	defer tc.Close()
	resp := tc.Get("/int", 5)
	resp.Expect.Status.ToBe(200)
	resp.Expect.BodyStr.ToBe("5")
}

func TestIntListBody(t *testing.T) {
	tc := TC(t)
	defer tc.Close()
	resp := tc.Get("/intList", []int{5, 3, 8})
	resp.Expect.Status.ToBe(200)
	resp.Expect.BodyStr.ToBe("[5,3,8]")
}

func TestObjectBody(t *testing.T) {
	tc := TC(t)
	defer tc.Close()
	resp := tc.Get("/object", &calls.GetObjectBody{
		Name:  "Foo",
		Count: 7,
	})
	resp.Expect.Status.ToBe(200)
	resp.Expect.BodyStr.ToBe(`{"count":7,"name":"Foo"}`)
}

func TestObjectDefBody(t *testing.T) {
	tc := TC(t)
	defer tc.Close()
	resp := tc.Get("/objectDef", &calls.Obj{
		Name:  "Foo",
		Count: 7,
		Enum:  "e1a",
		Data:  3.14,
	})
	resp.Expect.Status.ToBe(200)
	resp.Expect.BodyStr.ToBe(`{"count":7,"name":"Foo","enum":"e1a","data":3.14}`)
}

func TestNullable(t *testing.T) {
	tc := TC(t)
	defer tc.Close()
	str := "foo"
	resp := tc.Get("/nullable", &calls.Nullable{Name: &str})
	resp.Expect.Status.ToBe(200)
	resp.Expect.BodyStr.ToBe(`{"name":"foo"}`)
}

func TestEnumBody(t *testing.T) {
	tc := TC(t)
	defer tc.Close()
	resp := tc.Get("/enum", "e1a")
	resp.Expect.Status.ToBe(200)
	resp.Expect.BodyStr.ToBe(`"e1a"`)
}

func TestWrongEnumBody(t *testing.T) {
	tc := TC(t)
	defer tc.Close()
	resp := tc.Get("/enum", "e1z")
	resp.Expect.Status.ToBe(http.StatusBadRequest)
}

func TestMissingBody(t *testing.T) {
	tc := TC(t)
	defer tc.Close()
	resp := tc.Get("/enum", nil)
	resp.Expect.Status.ToBe(http.StatusBadRequest)
	resp.Expect.BodyStr.ToBe("bad request, expected a body but none was received")
}
