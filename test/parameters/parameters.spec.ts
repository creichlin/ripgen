import { expect } from 'chai'
import { Client } from './ts/client.gen'
import fetch from 'node-fetch'

const api = new Client<string, Date>(
  'http://localhost:8000/parameters',
  async (url, method, body) => {
    return await fetch(url, { method, body })
  },
  (s) => new Date(s),
  (d) => d.toISOString()
)

describe('parameters', function () {
  describe('using int', function () {
    it('is number', async function () {
      expect(await api.getInt(7, 9)).to.eql([7, 9])
    })
  })
  describe('using time', function () {
    it("it's the same time", async function () {
      expect(
        await api.getTime(
          new Date(2020, 2, 5, 12, 10, 12),
          new Date(2020, 3, 5, 12, 0, 0)
        )
      ).to.eql([
        new Date(2020, 2, 5, 12, 10, 12),
        new Date(2020, 3, 5, 12, 0, 0)
      ])
    })
  })
})
