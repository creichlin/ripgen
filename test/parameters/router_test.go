package parameters

import (
	"net/http"
	"testing"

	"gitlab.com/creichlin/ripgen/test/internal/test"
)

func TC(t *testing.T) *test.TC {
	return test.New(t, CreateRouter())
}

func TestIntAsParameter(t *testing.T) {
	tc := TC(t)
	defer tc.Close()
	resp := tc.Get("/int/5?paramInt=10", nil)
	resp.Expect.Status.ToBe(200)
	resp.Expect.BodyStr.ToBe("[5,10]")
}

func TestIntAsWrongPathParameter(t *testing.T) {
	tc := TC(t)
	defer tc.Close()
	resp := tc.Get("/int/5a?paramInt=10", nil)
	resp.Expect.Status.ToBe(http.StatusBadRequest)
	resp.Expect.BodyStr.ToBe("bad request, expected parameter pathInt '5a' to be of type int")
}

func TestIntAsWrongQueryParameter(t *testing.T) {
	tc := TC(t)
	defer tc.Close()
	resp := tc.Get("/int/5?paramInt=fish", nil)
	resp.Expect.Status.ToBe(http.StatusBadRequest)
	resp.Expect.BodyStr.ToBe("bad request, expected parameter paramInt 'fish' to be of type int")
}

func TestBoolAsParameter(t *testing.T) {
	tc := TC(t)
	defer tc.Close()
	resp := tc.Get("/bool/true?paramBool=false", nil)
	resp.Expect.Status.ToBe(200)
	resp.Expect.BodyStr.ToBe("[true,false]")
}

func TestBoolAsWrongPathParameter(t *testing.T) {
	tc := TC(t)
	defer tc.Close()
	resp := tc.Get("/bool/tr?paramBool=false", nil)
	resp.Expect.Status.ToBe(http.StatusBadRequest)
	resp.Expect.BodyStr.ToBe("bad request, expected parameter pathBool 'tr' to be of type bool")
}

func TestBoolAsWrongQueryParameter(t *testing.T) {
	tc := TC(t)
	defer tc.Close()
	resp := tc.Get("/bool/false?paramBool=falafel", nil)
	resp.Expect.Status.ToBe(http.StatusBadRequest)
	resp.Expect.BodyStr.ToBe("bad request, expected parameter paramBool 'falafel' to be of type bool")
}

func TestTimeAsParameter(t *testing.T) {
	tc := TC(t)
	defer tc.Close()
	resp := tc.Get("/time/2011-08-30T13:22:53.108Z?paramTime=2014-03-03T17:10:00.000Z", nil)
	resp.Expect.Status.ToBe(200)
	resp.Expect.BodyStr.ToBe(`["2011-08-30T13:22:53.108Z","2014-03-03T17:10:00Z"]`)
}

func TestTimeAsWrongPathParameter(t *testing.T) {
	tc := TC(t)
	defer tc.Close()
	resp := tc.Get("/time/2011-08-30T13:22?paramTime=2014-03-03T17:10:00.000Z", nil)
	resp.Expect.Status.ToBe(http.StatusBadRequest)
	resp.Expect.BodyStr.ToBe("bad request, expected parameter pathTime '2011-08-30T13:22' to be of type time in format RFC3339")
}

func TestStringAsParameter(t *testing.T) {
	tc := TC(t)
	defer tc.Close()
	resp := tc.Get("/str/a lion?paramStr=the%20foo", nil)
	resp.Expect.Status.ToBe(200)
	resp.Expect.BodyStr.ToBe(`["a lion","the foo"]`)
}

func TestEnumAsPathParameter(t *testing.T) {
	tc := TC(t)
	defer tc.Close()
	resp := tc.Get("/enum/e1a?paramEnum=007", nil)
	resp.Expect.Status.ToBe(200)
	resp.Expect.BodyStr.ToBe(`["e1a","007"]`)
}

func TestEnumAsWrongPathParameter(t *testing.T) {
	tc := TC(t)
	defer tc.Close()
	resp := tc.Get("/enum/e1x?paramEnum=007", nil)
	resp.Expect.Status.ToBe(http.StatusBadRequest)
	resp.Expect.BodyStr.ToBe(`bad request, expected parameter pathEnum 'e1x' to be one of 'e1a', '', '007'`)
}

func TestEnumAsMissingParameter(t *testing.T) {
	tc := TC(t)
	defer tc.Close()
	resp := tc.Get("/enum/e1x", nil)
	resp.Expect.Status.ToBe(http.StatusBadRequest)
	resp.Expect.BodyStr.ToBe(`bad request, expected parameter pathEnum 'e1x' to be one of 'e1a', '', '007'`)
}
