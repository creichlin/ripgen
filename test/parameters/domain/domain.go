package domain

import (
	"time"

	"gitlab.com/creichlin/ripgen/test/parameters/calls"
)

type Domain struct {
}

func (d *Domain) GetInt(pathInt int, paramInt int) ([]int, error) {
	return []int{pathInt, paramInt}, nil
}

func (d *Domain) GetString(pathStr string, paramStr string) ([]string, error) {
	return []string{pathStr, paramStr}, nil
}

func (d *Domain) GetEnum(pathEnum calls.Enum1, paramEnum calls.Enum1) ([]string, error) {
	return []string{string(pathEnum), string(paramEnum)}, nil
}

func (d *Domain) GetTime(pathTime time.Time, paramTime time.Time) ([]time.Time, error) {
	return []time.Time{pathTime, paramTime}, nil
}

func (d *Domain) GetBool(pathBool bool, paramBool bool) ([]bool, error) {
	return []bool{pathBool, paramBool}, nil
}
