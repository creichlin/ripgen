package parameters

import (
	"fmt"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/creichlin/ripgen"
	"gitlab.com/creichlin/ripgen/test/parameters/calls"
	"gitlab.com/creichlin/ripgen/test/parameters/domain"
	"gitlab.com/creichlin/ripgen/test/parameters/hprof"
	"gitlab.com/creichlin/ripgen/test/parameters/httph"
)

func callsF(w http.ResponseWriter, r *http.Request, p httprouter.Params) (calls.Calls, error) {
	return hprof.Profile(&domain.Domain{}, func(i hprof.Invocation) {
		fmt.Println(i)
	}), nil
}

func CreateRouter() http.Handler {
	return httph.CreateRouter(callsF, ripgen.DefaultErrorHandler)
}
