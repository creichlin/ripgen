package domain

import (
	"io"

	"gitlab.com/creichlin/ripgen/test/response/calls"
)

type Domain struct {
}

func (d *Domain) ReturnInt(pathInt int) (int, error) {
	return pathInt, nil
}

func (d *Domain) ReturnFloat(pathInt float64) (float64, error) {
	return pathInt, nil
}

func (d *Domain) ReturnIntList() ([]int, error) {
	return []int{0, 5, 10, -1000}, nil
}

func (d *Domain) ReturnString(pathStr string) (string, error) {
	return pathStr, nil
}
func (d *Domain) ReturnStringList() ([]string, error) {
	return []string{"zab", "foo", "b", "a", `"`, "?"}, nil
}

func (d *Domain) GetEnum(pathEnum calls.Enum2) (calls.Enum2, error) {
	return pathEnum, nil
}
func (d *Domain) GetEnumList() ([]calls.Enum2, error) {
	return []calls.Enum2{calls.Enum2E1a, calls.Enum2E1c, calls.Enum2E1a}, nil
}

func (d *Domain) GetObject() (calls.GetObjectResponse, error) {
	return calls.GetObjectResponse{
		Name:  "foo",
		Count: 12,
		T:     calls.Enum2E1a,
	}, nil
}
func (d *Domain) GetObjectList() ([]calls.RespObj, error) {
	return []calls.RespObj{
		{
			Name:  "foo",
			Count: 12,
			T:     calls.Enum2E1a,
		},
		{
			Name:  "bar",
			Count: -10,
			T:     calls.Enum2E1c,
		},
	}, nil
}

func (d *Domain) GetBinary() ([]byte, error) {
	return []byte("foo"), nil
}

func (d *Domain) GetStream(stream io.Writer) error {
	_, err := stream.Write([]byte("foo"))
	return err
}

func (d *Domain) GetSVGStream(stream io.Writer) error {
	_, err := stream.Write([]byte("<svg></svg>"))
	return err
}
