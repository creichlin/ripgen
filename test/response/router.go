package response

import (
	"fmt"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/creichlin/ripgen"
	"gitlab.com/creichlin/ripgen/test/response/calls"
	"gitlab.com/creichlin/ripgen/test/response/domain"
	"gitlab.com/creichlin/ripgen/test/response/hprof"
	"gitlab.com/creichlin/ripgen/test/response/httph"
)

func callsF(w http.ResponseWriter, r *http.Request, p httprouter.Params) (calls.Calls, error) {
	return hprof.Profile(&domain.Domain{}, func(i hprof.Invocation) {
		fmt.Println(i)
	}), nil
}

func CreateRouter() http.Handler {
	return httph.CreateRouter(callsF, ripgen.DefaultErrorHandler)
}
