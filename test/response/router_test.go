package response

import (
	"testing"

	"gitlab.com/creichlin/ripgen/test/internal/test"
)

func TC(t *testing.T) *test.TC {
	return test.New(t, CreateRouter())
}

func TestIntResponse(t *testing.T) {
	tc := TC(t)
	defer tc.Close()
	resp := tc.Get("/int/5", nil)
	resp.Expect.Status.ToBe(200)
	resp.Expect.BodyStr.ToBe("5")
}

func TestIntListResponse(t *testing.T) {
	tc := TC(t)
	defer tc.Close()
	resp := tc.Get("/intlist", nil)
	resp.Expect.Status.ToBe(200)
	resp.Expect.BodyStr.ToBe("[0,5,10,-1000]")
}

func TestStringResponse(t *testing.T) {
	tc := TC(t)
	defer tc.Close()
	resp := tc.Get("/str/foo bar", nil)
	resp.Expect.Status.ToBe(200)
	resp.Expect.BodyStr.ToBe(`"foo bar"`)
}

func TestStringListResponse(t *testing.T) {
	tc := TC(t)
	defer tc.Close()
	resp := tc.Get("/strlist", nil)
	resp.Expect.Status.ToBe(200)
	resp.Expect.BodyStr.ToBe(`["zab","foo","b","a","\"","?"]`)
}

func TestEnumResponse(t *testing.T) {
	tc := TC(t)
	defer tc.Close()
	resp := tc.Get("/enum/e1a", nil)
	resp.Expect.Status.ToBe(200)
	resp.Expect.BodyStr.ToBe(`"e1a"`)
}

func TestEnumListResponse(t *testing.T) {
	tc := TC(t)
	defer tc.Close()
	resp := tc.Get("/enumlist", nil)
	resp.Expect.Status.ToBe(200)
	resp.Expect.BodyStr.ToBe(`["e1a","e1c","e1a"]`)
}

func TestObjectResponse(t *testing.T) {
	tc := TC(t)
	defer tc.Close()
	resp := tc.Get("/obj", nil)
	resp.Expect.Status.ToBe(200)
	resp.Expect.BodyStr.ToBe(`{"name":"foo","count":12,"t":"e1a"}`)
}
func TestObjectListResponse(t *testing.T) {
	tc := TC(t)
	defer tc.Close()
	resp := tc.Get("/objList", nil)
	resp.Expect.Status.ToBe(200)
	resp.Expect.BodyStr.ToBe(`[{"name":"foo","count":12,"t":"e1a"},{"name":"bar","count":-10,"t":"e1c"}]`)
}
