package test

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/testle/expect"
)

type TC struct {
	t *testing.T
	s *httptest.Server
	c *http.Client
}

func New(t *testing.T, handler http.Handler) *TC {
	tc := &TC{
		t: t,
		s: httptest.NewServer(handler),
		c: &http.Client{},
	}

	return tc
}

func (tc *TC) Close() {
	tc.s.Close()
}

func (tc *TC) Get(url string, body interface{}) Response {
	var bodyBuffer *bytes.Buffer
	var bodyBufferReader io.Reader
	if body != nil {
		jsonBytes, err := json.Marshal(body)
		if err != nil {
			tc.t.Fatal(err)
		}
		bodyBuffer = bytes.NewBuffer(jsonBytes)
		bodyBufferReader = bodyBuffer
	}

	req, err := http.NewRequest("GET", tc.s.URL+url, bodyBufferReader)
	if err != nil {
		tc.t.Fatal(err)
	}

	resp, err := tc.c.Do(req)
	if err != nil {
		tc.t.Fatal(err)
	}
	d, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		d = []byte{}
	}
	return Response{
		Expect: ExpectResponse{
			Status:  expect.Value(tc.t, "status", resp.StatusCode),
			BodyStr: expect.Value(tc.t, "body as string", string(d)),
		},
	}

}

type Response struct {
	Expect ExpectResponse
}

type ExpectResponse struct {
	Status  expect.Val
	BodyStr expect.Val
}
