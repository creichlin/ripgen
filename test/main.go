package main

import (
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strings"

	"gitlab.com/creichlin/ripgen/test/parameters"
	"gitlab.com/creichlin/ripgen/test/response"
)

func main() {
	sub := map[string]http.Handler{
		"response":   response.CreateRouter(),
		"parameters": parameters.CreateRouter(),
	}

	router := func(w http.ResponseWriter, r *http.Request) {
		p := strings.Split(r.URL.Path, "/")
		sr, has := sub[p[1]]
		if has {
			// make a copy of request with the first path part removed
			r2 := &http.Request{}
			*r2 = *r
			r2.URL = &url.URL{}
			*r2.URL = *r.URL
			r2.URL.Path = "/" + strings.Join(p[2:], "/")
			sr.ServeHTTP(w, r2)
			return
		}
		log.Printf("no handler found for path %v", r.URL.Path)
		w.WriteHeader(http.StatusInternalServerError)
	}

	fmt.Println("listening on :8000")
	log.Fatal(http.ListenAndServe(":8000", http.HandlerFunc(router)))
}
