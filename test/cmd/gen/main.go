package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path"
	"path/filepath"
	"runtime"

	"gitlab.com/creichlin/ripgen"
)

func main() {
	_, filename, _, _ := runtime.Caller(0)
	rips := path.Dir(path.Dir(path.Dir(filename)))

	files, err := ioutil.ReadDir(rips)
	if err != nil {
		log.Fatal(err)
	}

	for _, f := range files {
		if f.IsDir() {
			data, err := ioutil.ReadFile(filepath.Join(rips, f.Name(), f.Name()+".rip"))
			if os.IsNotExist(err) {
				continue
			}
			if err != nil {
				log.Fatal(err)
			}

			goRootPkg := filepath.Join(rips, f.Name())

			err = ripgen.GenerateGoInterface(data, goRootPkg+"/calls")
			if err != nil {
				log.Fatal(err)
			}
			err = ripgen.GenerateGoServer(data, goRootPkg+"/httph", goRootPkg+"/calls")
			if err != nil {
				log.Fatal(err)
			}
			err = ripgen.GenerateGoProfiler(data, goRootPkg+"/hprof", goRootPkg+"/calls")
			if err != nil {
				log.Fatal(err)
			}
			err = ripgen.GenerateGoClient(data, goRootPkg+"/client", goRootPkg+"/calls")
			if err != nil {
				log.Fatal(err)
			}
			err = ripgen.GenerateTypescriptTypes(data, filepath.Join(rips, f.Name(), "ts", "types.gen.d.ts"))
			if err != nil {
				log.Fatal(err)
			}
			err = ripgen.GenerateTypescriptClient(data, filepath.Join(rips, f.Name(), "ts", "client.gen.ts"))
			if err != nil {
				log.Fatal(err)
			}
		}
	}
	fmt.Println("generated test handlers")
}
