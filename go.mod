module gitlab.com/creichlin/ripgen

go 1.13

require (
	github.com/antlr/antlr4 v0.0.0-20201025162310-ac92c48a09df
	github.com/julienschmidt/httprouter v1.3.0
	gitlab.com/akabio/fmtid v0.1.2
	gitlab.com/akabio/iotool v0.1.0
	gitlab.com/creichlin/gogen v0.0.0-20201103160732-4f71b6cb77c7
	gitlab.com/creichlin/gopath v0.0.0-20201116202415-cc59f9114d51
	gitlab.com/testle/expect v0.0.0-20201116201311-ded3906ba518
	golang.org/x/tools v0.0.0-20201026223136-e84cfc6dd5ca
)
