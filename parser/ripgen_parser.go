// Code generated from parser/Ripgen.g4 by ANTLR 4.8. DO NOT EDIT.

package parser // Ripgen

import (
	"fmt"
	"reflect"
	"strconv"

	"github.com/antlr/antlr4/runtime/Go/antlr"
)

// Suppress unused import errors
var _ = fmt.Printf
var _ = reflect.Copy
var _ = strconv.Itoa

var parserATN = []uint16{
	3, 24715, 42794, 33075, 47597, 16764, 15335, 30598, 22884, 3, 40, 238,
	4, 2, 9, 2, 4, 3, 9, 3, 4, 4, 9, 4, 4, 5, 9, 5, 4, 6, 9, 6, 4, 7, 9, 7,
	4, 8, 9, 8, 4, 9, 9, 9, 4, 10, 9, 10, 4, 11, 9, 11, 4, 12, 9, 12, 4, 13,
	9, 13, 4, 14, 9, 14, 4, 15, 9, 15, 4, 16, 9, 16, 4, 17, 9, 17, 4, 18, 9,
	18, 4, 19, 9, 19, 4, 20, 9, 20, 4, 21, 9, 21, 4, 22, 9, 22, 4, 23, 9, 23,
	4, 24, 9, 24, 4, 25, 9, 25, 4, 26, 9, 26, 4, 27, 9, 27, 4, 28, 9, 28, 4,
	29, 9, 29, 4, 30, 9, 30, 4, 31, 9, 31, 4, 32, 9, 32, 4, 33, 9, 33, 4, 34,
	9, 34, 3, 2, 3, 2, 3, 2, 3, 3, 3, 3, 3, 4, 3, 4, 3, 5, 3, 5, 5, 5, 78,
	10, 5, 3, 6, 3, 6, 3, 6, 3, 6, 3, 6, 7, 6, 85, 10, 6, 12, 6, 14, 6, 88,
	11, 6, 3, 7, 3, 7, 7, 7, 92, 10, 7, 12, 7, 14, 7, 95, 11, 7, 3, 8, 3, 8,
	6, 8, 99, 10, 8, 13, 8, 14, 8, 100, 3, 8, 3, 8, 3, 8, 3, 8, 7, 8, 107,
	10, 8, 12, 8, 14, 8, 110, 11, 8, 5, 8, 112, 10, 8, 3, 8, 3, 8, 3, 8, 5,
	8, 117, 10, 8, 3, 8, 3, 8, 3, 8, 5, 8, 122, 10, 8, 3, 8, 3, 8, 3, 9, 3,
	9, 3, 9, 3, 9, 3, 10, 3, 10, 3, 10, 3, 10, 3, 11, 3, 11, 3, 11, 3, 11,
	3, 12, 3, 12, 3, 12, 3, 12, 3, 13, 3, 13, 3, 13, 3, 13, 3, 14, 3, 14, 3,
	15, 3, 15, 3, 15, 3, 15, 3, 16, 3, 16, 3, 16, 3, 16, 3, 16, 3, 16, 5, 16,
	158, 10, 16, 3, 17, 3, 17, 3, 17, 3, 18, 3, 18, 3, 18, 3, 18, 3, 19, 3,
	19, 5, 19, 169, 10, 19, 3, 20, 3, 20, 5, 20, 173, 10, 20, 3, 21, 3, 21,
	3, 21, 3, 21, 3, 21, 3, 21, 3, 21, 3, 21, 3, 21, 5, 21, 184, 10, 21, 3,
	22, 3, 22, 3, 22, 5, 22, 189, 10, 22, 3, 23, 3, 23, 3, 24, 3, 24, 3, 25,
	3, 25, 3, 26, 3, 26, 3, 27, 3, 27, 3, 28, 3, 28, 3, 29, 3, 29, 3, 29, 3,
	29, 3, 30, 3, 30, 3, 30, 3, 30, 3, 31, 3, 31, 7, 31, 213, 10, 31, 12, 31,
	14, 31, 216, 11, 31, 3, 31, 3, 31, 3, 32, 3, 32, 3, 32, 3, 32, 3, 32, 7,
	32, 225, 10, 32, 12, 32, 14, 32, 228, 11, 32, 3, 32, 3, 32, 3, 33, 3, 33,
	5, 33, 234, 10, 33, 3, 34, 3, 34, 3, 34, 2, 2, 35, 2, 4, 6, 8, 10, 12,
	14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46, 48,
	50, 52, 54, 56, 58, 60, 62, 64, 66, 2, 5, 5, 2, 3, 15, 29, 32, 38, 38,
	3, 2, 17, 19, 3, 2, 29, 32, 2, 231, 2, 68, 3, 2, 2, 2, 4, 71, 3, 2, 2,
	2, 6, 73, 3, 2, 2, 2, 8, 77, 3, 2, 2, 2, 10, 79, 3, 2, 2, 2, 12, 93, 3,
	2, 2, 2, 14, 96, 3, 2, 2, 2, 16, 125, 3, 2, 2, 2, 18, 129, 3, 2, 2, 2,
	20, 133, 3, 2, 2, 2, 22, 137, 3, 2, 2, 2, 24, 141, 3, 2, 2, 2, 26, 145,
	3, 2, 2, 2, 28, 147, 3, 2, 2, 2, 30, 157, 3, 2, 2, 2, 32, 159, 3, 2, 2,
	2, 34, 162, 3, 2, 2, 2, 36, 166, 3, 2, 2, 2, 38, 170, 3, 2, 2, 2, 40, 183,
	3, 2, 2, 2, 42, 188, 3, 2, 2, 2, 44, 190, 3, 2, 2, 2, 46, 192, 3, 2, 2,
	2, 48, 194, 3, 2, 2, 2, 50, 196, 3, 2, 2, 2, 52, 198, 3, 2, 2, 2, 54, 200,
	3, 2, 2, 2, 56, 202, 3, 2, 2, 2, 58, 206, 3, 2, 2, 2, 60, 210, 3, 2, 2,
	2, 62, 219, 3, 2, 2, 2, 64, 231, 3, 2, 2, 2, 66, 235, 3, 2, 2, 2, 68, 69,
	5, 12, 7, 2, 69, 70, 7, 2, 2, 3, 70, 3, 3, 2, 2, 2, 71, 72, 7, 37, 2, 2,
	72, 5, 3, 2, 2, 2, 73, 74, 9, 2, 2, 2, 74, 7, 3, 2, 2, 2, 75, 78, 5, 4,
	3, 2, 76, 78, 5, 6, 4, 2, 77, 75, 3, 2, 2, 2, 77, 76, 3, 2, 2, 2, 78, 9,
	3, 2, 2, 2, 79, 80, 5, 8, 5, 2, 80, 81, 7, 16, 2, 2, 81, 86, 5, 8, 5, 2,
	82, 83, 9, 3, 2, 2, 83, 85, 5, 8, 5, 2, 84, 82, 3, 2, 2, 2, 85, 88, 3,
	2, 2, 2, 86, 84, 3, 2, 2, 2, 86, 87, 3, 2, 2, 2, 87, 11, 3, 2, 2, 2, 88,
	86, 3, 2, 2, 2, 89, 92, 5, 34, 18, 2, 90, 92, 5, 14, 8, 2, 91, 89, 3, 2,
	2, 2, 91, 90, 3, 2, 2, 2, 92, 95, 3, 2, 2, 2, 93, 91, 3, 2, 2, 2, 93, 94,
	3, 2, 2, 2, 94, 13, 3, 2, 2, 2, 95, 93, 3, 2, 2, 2, 96, 98, 5, 26, 14,
	2, 97, 99, 5, 30, 16, 2, 98, 97, 3, 2, 2, 2, 99, 100, 3, 2, 2, 2, 100,
	98, 3, 2, 2, 2, 100, 101, 3, 2, 2, 2, 101, 111, 3, 2, 2, 2, 102, 103, 7,
	35, 2, 2, 103, 108, 5, 28, 15, 2, 104, 105, 7, 20, 2, 2, 105, 107, 5, 28,
	15, 2, 106, 104, 3, 2, 2, 2, 107, 110, 3, 2, 2, 2, 108, 106, 3, 2, 2, 2,
	108, 109, 3, 2, 2, 2, 109, 112, 3, 2, 2, 2, 110, 108, 3, 2, 2, 2, 111,
	102, 3, 2, 2, 2, 111, 112, 3, 2, 2, 2, 112, 113, 3, 2, 2, 2, 113, 114,
	7, 21, 2, 2, 114, 116, 5, 16, 9, 2, 115, 117, 5, 18, 10, 2, 116, 115, 3,
	2, 2, 2, 116, 117, 3, 2, 2, 2, 117, 121, 3, 2, 2, 2, 118, 122, 5, 20, 11,
	2, 119, 122, 5, 22, 12, 2, 120, 122, 5, 24, 13, 2, 121, 118, 3, 2, 2, 2,
	121, 119, 3, 2, 2, 2, 121, 120, 3, 2, 2, 2, 121, 122, 3, 2, 2, 2, 122,
	123, 3, 2, 2, 2, 123, 124, 7, 22, 2, 2, 124, 15, 3, 2, 2, 2, 125, 126,
	7, 11, 2, 2, 126, 127, 7, 33, 2, 2, 127, 128, 5, 6, 4, 2, 128, 17, 3, 2,
	2, 2, 129, 130, 7, 12, 2, 2, 130, 131, 7, 33, 2, 2, 131, 132, 5, 38, 20,
	2, 132, 19, 3, 2, 2, 2, 133, 134, 7, 13, 2, 2, 134, 135, 7, 33, 2, 2, 135,
	136, 5, 38, 20, 2, 136, 21, 3, 2, 2, 2, 137, 138, 7, 14, 2, 2, 138, 139,
	7, 33, 2, 2, 139, 140, 5, 10, 6, 2, 140, 23, 3, 2, 2, 2, 141, 142, 7, 15,
	2, 2, 142, 143, 7, 33, 2, 2, 143, 144, 5, 10, 6, 2, 144, 25, 3, 2, 2, 2,
	145, 146, 9, 4, 2, 2, 146, 27, 3, 2, 2, 2, 147, 148, 7, 21, 2, 2, 148,
	149, 5, 32, 17, 2, 149, 150, 7, 22, 2, 2, 150, 29, 3, 2, 2, 2, 151, 152,
	7, 16, 2, 2, 152, 158, 5, 8, 5, 2, 153, 154, 7, 23, 2, 2, 154, 155, 5,
	32, 17, 2, 155, 156, 7, 22, 2, 2, 156, 158, 3, 2, 2, 2, 157, 151, 3, 2,
	2, 2, 157, 153, 3, 2, 2, 2, 158, 31, 3, 2, 2, 2, 159, 160, 5, 6, 4, 2,
	160, 161, 5, 36, 19, 2, 161, 33, 3, 2, 2, 2, 162, 163, 7, 10, 2, 2, 163,
	164, 5, 4, 3, 2, 164, 165, 5, 42, 22, 2, 165, 35, 3, 2, 2, 2, 166, 168,
	5, 40, 21, 2, 167, 169, 7, 35, 2, 2, 168, 167, 3, 2, 2, 2, 168, 169, 3,
	2, 2, 2, 169, 37, 3, 2, 2, 2, 170, 172, 5, 42, 22, 2, 171, 173, 7, 35,
	2, 2, 172, 171, 3, 2, 2, 2, 172, 173, 3, 2, 2, 2, 173, 39, 3, 2, 2, 2,
	174, 184, 5, 44, 23, 2, 175, 184, 5, 46, 24, 2, 176, 184, 5, 52, 27, 2,
	177, 184, 5, 56, 29, 2, 178, 184, 5, 58, 30, 2, 179, 184, 5, 50, 26, 2,
	180, 184, 5, 48, 25, 2, 181, 184, 5, 54, 28, 2, 182, 184, 5, 66, 34, 2,
	183, 174, 3, 2, 2, 2, 183, 175, 3, 2, 2, 2, 183, 176, 3, 2, 2, 2, 183,
	177, 3, 2, 2, 2, 183, 178, 3, 2, 2, 2, 183, 179, 3, 2, 2, 2, 183, 180,
	3, 2, 2, 2, 183, 181, 3, 2, 2, 2, 183, 182, 3, 2, 2, 2, 184, 41, 3, 2,
	2, 2, 185, 189, 5, 60, 31, 2, 186, 189, 5, 62, 32, 2, 187, 189, 5, 40,
	21, 2, 188, 185, 3, 2, 2, 2, 188, 186, 3, 2, 2, 2, 188, 187, 3, 2, 2, 2,
	189, 43, 3, 2, 2, 2, 190, 191, 7, 3, 2, 2, 191, 45, 3, 2, 2, 2, 192, 193,
	7, 4, 2, 2, 193, 47, 3, 2, 2, 2, 194, 195, 7, 6, 2, 2, 195, 49, 3, 2, 2,
	2, 196, 197, 7, 7, 2, 2, 197, 51, 3, 2, 2, 2, 198, 199, 7, 5, 2, 2, 199,
	53, 3, 2, 2, 2, 200, 201, 7, 9, 2, 2, 201, 55, 3, 2, 2, 2, 202, 203, 7,
	24, 2, 2, 203, 204, 5, 36, 19, 2, 204, 205, 7, 25, 2, 2, 205, 57, 3, 2,
	2, 2, 206, 207, 7, 21, 2, 2, 207, 208, 5, 36, 19, 2, 208, 209, 7, 22, 2,
	2, 209, 59, 3, 2, 2, 2, 210, 214, 7, 21, 2, 2, 211, 213, 5, 32, 17, 2,
	212, 211, 3, 2, 2, 2, 213, 216, 3, 2, 2, 2, 214, 212, 3, 2, 2, 2, 214,
	215, 3, 2, 2, 2, 215, 217, 3, 2, 2, 2, 216, 214, 3, 2, 2, 2, 217, 218,
	7, 22, 2, 2, 218, 61, 3, 2, 2, 2, 219, 220, 7, 8, 2, 2, 220, 221, 7, 26,
	2, 2, 221, 226, 5, 64, 33, 2, 222, 223, 7, 27, 2, 2, 223, 225, 5, 64, 33,
	2, 224, 222, 3, 2, 2, 2, 225, 228, 3, 2, 2, 2, 226, 224, 3, 2, 2, 2, 226,
	227, 3, 2, 2, 2, 227, 229, 3, 2, 2, 2, 228, 226, 3, 2, 2, 2, 229, 230,
	7, 28, 2, 2, 230, 63, 3, 2, 2, 2, 231, 233, 5, 6, 4, 2, 232, 234, 7, 39,
	2, 2, 233, 232, 3, 2, 2, 2, 233, 234, 3, 2, 2, 2, 234, 65, 3, 2, 2, 2,
	235, 236, 5, 4, 3, 2, 236, 67, 3, 2, 2, 2, 19, 77, 86, 91, 93, 100, 108,
	111, 116, 121, 157, 168, 172, 183, 188, 214, 226, 233,
}
var deserializer = antlr.NewATNDeserializer(nil)
var deserializedATN = deserializer.DeserializeFromUInt16(parserATN)

var literalNames = []string{
	"", "'int'", "'float'", "'string'", "'time'", "'bool'", "'enum'", "'any'",
	"'type'", "'name'", "'body'", "'response'", "'binaryResponse'", "'streamResponse'",
	"'/'", "'+'", "'-'", "'.'", "'&'", "'{'", "'}'", "'/{'", "'['", "']'",
	"'('", "','", "')'", "'get'", "'post'", "'put'", "'delete'", "':'", "'*'",
	"'?'",
}
var symbolicNames = []string{
	"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
	"", "", "", "", "", "", "", "", "", "GET", "POST", "PUT", "DELETE", "COL",
	"STAR", "NULLABLE", "Comment", "TYPE_IDENTIFIER", "PROP_IDENTIFIER", "STRING_LITERAL",
	"WHITESPACE",
}

var ruleNames = []string{
	"start", "typeIdentifier", "propIdentifier", "identifier", "mimeType",
	"root", "call", "callName", "callBody", "callResponse", "callBinaryResponse",
	"callStreamResponse", "method", "urlParam", "pathPart", "namedType", "typeDeclaration",
	"subTypeSlot", "topTypeSlot", "subTypeDef", "topTypeDef", "intType", "floatType",
	"timeType", "boolType", "stringType", "anyType", "arrayType", "mapType",
	"objectType", "enumType", "enumValue", "customType",
}
var decisionToDFA = make([]*antlr.DFA, len(deserializedATN.DecisionToState))

func init() {
	for index, ds := range deserializedATN.DecisionToState {
		decisionToDFA[index] = antlr.NewDFA(ds, index)
	}
}

type RipgenParser struct {
	*antlr.BaseParser
}

func NewRipgenParser(input antlr.TokenStream) *RipgenParser {
	this := new(RipgenParser)

	this.BaseParser = antlr.NewBaseParser(input)

	this.Interpreter = antlr.NewParserATNSimulator(this, deserializedATN, decisionToDFA, antlr.NewPredictionContextCache())
	this.RuleNames = ruleNames
	this.LiteralNames = literalNames
	this.SymbolicNames = symbolicNames
	this.GrammarFileName = "Ripgen.g4"

	return this
}

// RipgenParser tokens.
const (
	RipgenParserEOF             = antlr.TokenEOF
	RipgenParserT__0            = 1
	RipgenParserT__1            = 2
	RipgenParserT__2            = 3
	RipgenParserT__3            = 4
	RipgenParserT__4            = 5
	RipgenParserT__5            = 6
	RipgenParserT__6            = 7
	RipgenParserT__7            = 8
	RipgenParserT__8            = 9
	RipgenParserT__9            = 10
	RipgenParserT__10           = 11
	RipgenParserT__11           = 12
	RipgenParserT__12           = 13
	RipgenParserT__13           = 14
	RipgenParserT__14           = 15
	RipgenParserT__15           = 16
	RipgenParserT__16           = 17
	RipgenParserT__17           = 18
	RipgenParserT__18           = 19
	RipgenParserT__19           = 20
	RipgenParserT__20           = 21
	RipgenParserT__21           = 22
	RipgenParserT__22           = 23
	RipgenParserT__23           = 24
	RipgenParserT__24           = 25
	RipgenParserT__25           = 26
	RipgenParserGET             = 27
	RipgenParserPOST            = 28
	RipgenParserPUT             = 29
	RipgenParserDELETE          = 30
	RipgenParserCOL             = 31
	RipgenParserSTAR            = 32
	RipgenParserNULLABLE        = 33
	RipgenParserComment         = 34
	RipgenParserTYPE_IDENTIFIER = 35
	RipgenParserPROP_IDENTIFIER = 36
	RipgenParserSTRING_LITERAL  = 37
	RipgenParserWHITESPACE      = 38
)

// RipgenParser rules.
const (
	RipgenParserRULE_start              = 0
	RipgenParserRULE_typeIdentifier     = 1
	RipgenParserRULE_propIdentifier     = 2
	RipgenParserRULE_identifier         = 3
	RipgenParserRULE_mimeType           = 4
	RipgenParserRULE_root               = 5
	RipgenParserRULE_call               = 6
	RipgenParserRULE_callName           = 7
	RipgenParserRULE_callBody           = 8
	RipgenParserRULE_callResponse       = 9
	RipgenParserRULE_callBinaryResponse = 10
	RipgenParserRULE_callStreamResponse = 11
	RipgenParserRULE_method             = 12
	RipgenParserRULE_urlParam           = 13
	RipgenParserRULE_pathPart           = 14
	RipgenParserRULE_namedType          = 15
	RipgenParserRULE_typeDeclaration    = 16
	RipgenParserRULE_subTypeSlot        = 17
	RipgenParserRULE_topTypeSlot        = 18
	RipgenParserRULE_subTypeDef         = 19
	RipgenParserRULE_topTypeDef         = 20
	RipgenParserRULE_intType            = 21
	RipgenParserRULE_floatType          = 22
	RipgenParserRULE_timeType           = 23
	RipgenParserRULE_boolType           = 24
	RipgenParserRULE_stringType         = 25
	RipgenParserRULE_anyType            = 26
	RipgenParserRULE_arrayType          = 27
	RipgenParserRULE_mapType            = 28
	RipgenParserRULE_objectType         = 29
	RipgenParserRULE_enumType           = 30
	RipgenParserRULE_enumValue          = 31
	RipgenParserRULE_customType         = 32
)

// IStartContext is an interface to support dynamic dispatch.
type IStartContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsStartContext differentiates from other interfaces.
	IsStartContext()
}

type StartContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyStartContext() *StartContext {
	var p = new(StartContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RipgenParserRULE_start
	return p
}

func (*StartContext) IsStartContext() {}

func NewStartContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *StartContext {
	var p = new(StartContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_start

	return p
}

func (s *StartContext) GetParser() antlr.Parser { return s.parser }

func (s *StartContext) Root() IRootContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IRootContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IRootContext)
}

func (s *StartContext) EOF() antlr.TerminalNode {
	return s.GetToken(RipgenParserEOF, 0)
}

func (s *StartContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *StartContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *StartContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitStart(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) Start() (localctx IStartContext) {
	localctx = NewStartContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 0, RipgenParserRULE_start)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(66)
		p.Root()
	}
	{
		p.SetState(67)
		p.Match(RipgenParserEOF)
	}

	return localctx
}

// ITypeIdentifierContext is an interface to support dynamic dispatch.
type ITypeIdentifierContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsTypeIdentifierContext differentiates from other interfaces.
	IsTypeIdentifierContext()
}

type TypeIdentifierContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyTypeIdentifierContext() *TypeIdentifierContext {
	var p = new(TypeIdentifierContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RipgenParserRULE_typeIdentifier
	return p
}

func (*TypeIdentifierContext) IsTypeIdentifierContext() {}

func NewTypeIdentifierContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *TypeIdentifierContext {
	var p = new(TypeIdentifierContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_typeIdentifier

	return p
}

func (s *TypeIdentifierContext) GetParser() antlr.Parser { return s.parser }

func (s *TypeIdentifierContext) TYPE_IDENTIFIER() antlr.TerminalNode {
	return s.GetToken(RipgenParserTYPE_IDENTIFIER, 0)
}

func (s *TypeIdentifierContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *TypeIdentifierContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *TypeIdentifierContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitTypeIdentifier(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) TypeIdentifier() (localctx ITypeIdentifierContext) {
	localctx = NewTypeIdentifierContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 2, RipgenParserRULE_typeIdentifier)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(69)
		p.Match(RipgenParserTYPE_IDENTIFIER)
	}

	return localctx
}

// IPropIdentifierContext is an interface to support dynamic dispatch.
type IPropIdentifierContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsPropIdentifierContext differentiates from other interfaces.
	IsPropIdentifierContext()
}

type PropIdentifierContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyPropIdentifierContext() *PropIdentifierContext {
	var p = new(PropIdentifierContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RipgenParserRULE_propIdentifier
	return p
}

func (*PropIdentifierContext) IsPropIdentifierContext() {}

func NewPropIdentifierContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *PropIdentifierContext {
	var p = new(PropIdentifierContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_propIdentifier

	return p
}

func (s *PropIdentifierContext) GetParser() antlr.Parser { return s.parser }

func (s *PropIdentifierContext) PROP_IDENTIFIER() antlr.TerminalNode {
	return s.GetToken(RipgenParserPROP_IDENTIFIER, 0)
}

func (s *PropIdentifierContext) GET() antlr.TerminalNode {
	return s.GetToken(RipgenParserGET, 0)
}

func (s *PropIdentifierContext) POST() antlr.TerminalNode {
	return s.GetToken(RipgenParserPOST, 0)
}

func (s *PropIdentifierContext) PUT() antlr.TerminalNode {
	return s.GetToken(RipgenParserPUT, 0)
}

func (s *PropIdentifierContext) DELETE() antlr.TerminalNode {
	return s.GetToken(RipgenParserDELETE, 0)
}

func (s *PropIdentifierContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *PropIdentifierContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *PropIdentifierContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitPropIdentifier(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) PropIdentifier() (localctx IPropIdentifierContext) {
	localctx = NewPropIdentifierContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 4, RipgenParserRULE_propIdentifier)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(71)
		_la = p.GetTokenStream().LA(1)

		if !((((_la)&-(0x1f+1)) == 0 && ((1<<uint(_la))&((1<<RipgenParserT__0)|(1<<RipgenParserT__1)|(1<<RipgenParserT__2)|(1<<RipgenParserT__3)|(1<<RipgenParserT__4)|(1<<RipgenParserT__5)|(1<<RipgenParserT__6)|(1<<RipgenParserT__7)|(1<<RipgenParserT__8)|(1<<RipgenParserT__9)|(1<<RipgenParserT__10)|(1<<RipgenParserT__11)|(1<<RipgenParserT__12)|(1<<RipgenParserGET)|(1<<RipgenParserPOST)|(1<<RipgenParserPUT)|(1<<RipgenParserDELETE))) != 0) || _la == RipgenParserPROP_IDENTIFIER) {
			p.GetErrorHandler().RecoverInline(p)
		} else {
			p.GetErrorHandler().ReportMatch(p)
			p.Consume()
		}
	}

	return localctx
}

// IIdentifierContext is an interface to support dynamic dispatch.
type IIdentifierContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsIdentifierContext differentiates from other interfaces.
	IsIdentifierContext()
}

type IdentifierContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyIdentifierContext() *IdentifierContext {
	var p = new(IdentifierContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RipgenParserRULE_identifier
	return p
}

func (*IdentifierContext) IsIdentifierContext() {}

func NewIdentifierContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *IdentifierContext {
	var p = new(IdentifierContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_identifier

	return p
}

func (s *IdentifierContext) GetParser() antlr.Parser { return s.parser }

func (s *IdentifierContext) TypeIdentifier() ITypeIdentifierContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ITypeIdentifierContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ITypeIdentifierContext)
}

func (s *IdentifierContext) PropIdentifier() IPropIdentifierContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IPropIdentifierContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IPropIdentifierContext)
}

func (s *IdentifierContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IdentifierContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *IdentifierContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitIdentifier(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) Identifier() (localctx IIdentifierContext) {
	localctx = NewIdentifierContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 6, RipgenParserRULE_identifier)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.SetState(75)
	p.GetErrorHandler().Sync(p)

	switch p.GetTokenStream().LA(1) {
	case RipgenParserTYPE_IDENTIFIER:
		p.EnterOuterAlt(localctx, 1)
		{
			p.SetState(73)
			p.TypeIdentifier()
		}

	case RipgenParserT__0, RipgenParserT__1, RipgenParserT__2, RipgenParserT__3, RipgenParserT__4, RipgenParserT__5, RipgenParserT__6, RipgenParserT__7, RipgenParserT__8, RipgenParserT__9, RipgenParserT__10, RipgenParserT__11, RipgenParserT__12, RipgenParserGET, RipgenParserPOST, RipgenParserPUT, RipgenParserDELETE, RipgenParserPROP_IDENTIFIER:
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(74)
			p.PropIdentifier()
		}

	default:
		panic(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
	}

	return localctx
}

// IMimeTypeContext is an interface to support dynamic dispatch.
type IMimeTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsMimeTypeContext differentiates from other interfaces.
	IsMimeTypeContext()
}

type MimeTypeContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyMimeTypeContext() *MimeTypeContext {
	var p = new(MimeTypeContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RipgenParserRULE_mimeType
	return p
}

func (*MimeTypeContext) IsMimeTypeContext() {}

func NewMimeTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *MimeTypeContext {
	var p = new(MimeTypeContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_mimeType

	return p
}

func (s *MimeTypeContext) GetParser() antlr.Parser { return s.parser }

func (s *MimeTypeContext) AllIdentifier() []IIdentifierContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IIdentifierContext)(nil)).Elem())
	var tst = make([]IIdentifierContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IIdentifierContext)
		}
	}

	return tst
}

func (s *MimeTypeContext) Identifier(i int) IIdentifierContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIdentifierContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *MimeTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *MimeTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *MimeTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitMimeType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) MimeType() (localctx IMimeTypeContext) {
	localctx = NewMimeTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 8, RipgenParserRULE_mimeType)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(77)
		p.Identifier()
	}
	{
		p.SetState(78)
		p.Match(RipgenParserT__13)
	}
	{
		p.SetState(79)
		p.Identifier()
	}
	p.SetState(84)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	for ((_la)&-(0x1f+1)) == 0 && ((1<<uint(_la))&((1<<RipgenParserT__14)|(1<<RipgenParserT__15)|(1<<RipgenParserT__16))) != 0 {
		{
			p.SetState(80)
			_la = p.GetTokenStream().LA(1)

			if !(((_la)&-(0x1f+1)) == 0 && ((1<<uint(_la))&((1<<RipgenParserT__14)|(1<<RipgenParserT__15)|(1<<RipgenParserT__16))) != 0) {
				p.GetErrorHandler().RecoverInline(p)
			} else {
				p.GetErrorHandler().ReportMatch(p)
				p.Consume()
			}
		}
		{
			p.SetState(81)
			p.Identifier()
		}

		p.SetState(86)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)
	}

	return localctx
}

// IRootContext is an interface to support dynamic dispatch.
type IRootContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsRootContext differentiates from other interfaces.
	IsRootContext()
}

type RootContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyRootContext() *RootContext {
	var p = new(RootContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RipgenParserRULE_root
	return p
}

func (*RootContext) IsRootContext() {}

func NewRootContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *RootContext {
	var p = new(RootContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_root

	return p
}

func (s *RootContext) GetParser() antlr.Parser { return s.parser }

func (s *RootContext) AllTypeDeclaration() []ITypeDeclarationContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*ITypeDeclarationContext)(nil)).Elem())
	var tst = make([]ITypeDeclarationContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(ITypeDeclarationContext)
		}
	}

	return tst
}

func (s *RootContext) TypeDeclaration(i int) ITypeDeclarationContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ITypeDeclarationContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(ITypeDeclarationContext)
}

func (s *RootContext) AllCall() []ICallContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*ICallContext)(nil)).Elem())
	var tst = make([]ICallContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(ICallContext)
		}
	}

	return tst
}

func (s *RootContext) Call(i int) ICallContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ICallContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(ICallContext)
}

func (s *RootContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *RootContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *RootContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitRoot(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) Root() (localctx IRootContext) {
	localctx = NewRootContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 10, RipgenParserRULE_root)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	p.SetState(91)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	for ((_la)&-(0x1f+1)) == 0 && ((1<<uint(_la))&((1<<RipgenParserT__7)|(1<<RipgenParserGET)|(1<<RipgenParserPOST)|(1<<RipgenParserPUT)|(1<<RipgenParserDELETE))) != 0 {
		p.SetState(89)
		p.GetErrorHandler().Sync(p)

		switch p.GetTokenStream().LA(1) {
		case RipgenParserT__7:
			{
				p.SetState(87)
				p.TypeDeclaration()
			}

		case RipgenParserGET, RipgenParserPOST, RipgenParserPUT, RipgenParserDELETE:
			{
				p.SetState(88)
				p.Call()
			}

		default:
			panic(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
		}

		p.SetState(93)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)
	}

	return localctx
}

// ICallContext is an interface to support dynamic dispatch.
type ICallContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsCallContext differentiates from other interfaces.
	IsCallContext()
}

type CallContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyCallContext() *CallContext {
	var p = new(CallContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RipgenParserRULE_call
	return p
}

func (*CallContext) IsCallContext() {}

func NewCallContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *CallContext {
	var p = new(CallContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_call

	return p
}

func (s *CallContext) GetParser() antlr.Parser { return s.parser }

func (s *CallContext) Method() IMethodContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IMethodContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IMethodContext)
}

func (s *CallContext) CallName() ICallNameContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ICallNameContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ICallNameContext)
}

func (s *CallContext) AllPathPart() []IPathPartContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IPathPartContext)(nil)).Elem())
	var tst = make([]IPathPartContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IPathPartContext)
		}
	}

	return tst
}

func (s *CallContext) PathPart(i int) IPathPartContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IPathPartContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IPathPartContext)
}

func (s *CallContext) NULLABLE() antlr.TerminalNode {
	return s.GetToken(RipgenParserNULLABLE, 0)
}

func (s *CallContext) CallBody() ICallBodyContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ICallBodyContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ICallBodyContext)
}

func (s *CallContext) CallResponse() ICallResponseContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ICallResponseContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ICallResponseContext)
}

func (s *CallContext) CallBinaryResponse() ICallBinaryResponseContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ICallBinaryResponseContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ICallBinaryResponseContext)
}

func (s *CallContext) CallStreamResponse() ICallStreamResponseContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ICallStreamResponseContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ICallStreamResponseContext)
}

func (s *CallContext) AllUrlParam() []IUrlParamContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IUrlParamContext)(nil)).Elem())
	var tst = make([]IUrlParamContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IUrlParamContext)
		}
	}

	return tst
}

func (s *CallContext) UrlParam(i int) IUrlParamContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IUrlParamContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IUrlParamContext)
}

func (s *CallContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *CallContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *CallContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitCall(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) Call() (localctx ICallContext) {
	localctx = NewCallContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 12, RipgenParserRULE_call)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(94)
		p.Method()
	}
	p.SetState(96)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	for ok := true; ok; ok = _la == RipgenParserT__13 || _la == RipgenParserT__20 {
		{
			p.SetState(95)
			p.PathPart()
		}

		p.SetState(98)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)
	}
	p.SetState(109)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	if _la == RipgenParserNULLABLE {
		{
			p.SetState(100)
			p.Match(RipgenParserNULLABLE)
		}

		{
			p.SetState(101)
			p.UrlParam()
		}

		p.SetState(106)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)

		for _la == RipgenParserT__17 {
			{
				p.SetState(102)
				p.Match(RipgenParserT__17)
			}
			{
				p.SetState(103)
				p.UrlParam()
			}

			p.SetState(108)
			p.GetErrorHandler().Sync(p)
			_la = p.GetTokenStream().LA(1)
		}

	}
	{
		p.SetState(111)
		p.Match(RipgenParserT__18)
	}
	{
		p.SetState(112)
		p.CallName()
	}
	p.SetState(114)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	if _la == RipgenParserT__9 {
		{
			p.SetState(113)
			p.CallBody()
		}

	}
	p.SetState(119)
	p.GetErrorHandler().Sync(p)

	switch p.GetTokenStream().LA(1) {
	case RipgenParserT__10:
		{
			p.SetState(116)
			p.CallResponse()
		}

	case RipgenParserT__11:
		{
			p.SetState(117)
			p.CallBinaryResponse()
		}

	case RipgenParserT__12:
		{
			p.SetState(118)
			p.CallStreamResponse()
		}

	case RipgenParserT__19:

	default:
	}
	{
		p.SetState(121)
		p.Match(RipgenParserT__19)
	}

	return localctx
}

// ICallNameContext is an interface to support dynamic dispatch.
type ICallNameContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsCallNameContext differentiates from other interfaces.
	IsCallNameContext()
}

type CallNameContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyCallNameContext() *CallNameContext {
	var p = new(CallNameContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RipgenParserRULE_callName
	return p
}

func (*CallNameContext) IsCallNameContext() {}

func NewCallNameContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *CallNameContext {
	var p = new(CallNameContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_callName

	return p
}

func (s *CallNameContext) GetParser() antlr.Parser { return s.parser }

func (s *CallNameContext) COL() antlr.TerminalNode {
	return s.GetToken(RipgenParserCOL, 0)
}

func (s *CallNameContext) PropIdentifier() IPropIdentifierContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IPropIdentifierContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IPropIdentifierContext)
}

func (s *CallNameContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *CallNameContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *CallNameContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitCallName(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) CallName() (localctx ICallNameContext) {
	localctx = NewCallNameContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 14, RipgenParserRULE_callName)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(123)
		p.Match(RipgenParserT__8)
	}
	{
		p.SetState(124)
		p.Match(RipgenParserCOL)
	}
	{
		p.SetState(125)
		p.PropIdentifier()
	}

	return localctx
}

// ICallBodyContext is an interface to support dynamic dispatch.
type ICallBodyContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsCallBodyContext differentiates from other interfaces.
	IsCallBodyContext()
}

type CallBodyContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyCallBodyContext() *CallBodyContext {
	var p = new(CallBodyContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RipgenParserRULE_callBody
	return p
}

func (*CallBodyContext) IsCallBodyContext() {}

func NewCallBodyContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *CallBodyContext {
	var p = new(CallBodyContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_callBody

	return p
}

func (s *CallBodyContext) GetParser() antlr.Parser { return s.parser }

func (s *CallBodyContext) COL() antlr.TerminalNode {
	return s.GetToken(RipgenParserCOL, 0)
}

func (s *CallBodyContext) TopTypeSlot() ITopTypeSlotContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ITopTypeSlotContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ITopTypeSlotContext)
}

func (s *CallBodyContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *CallBodyContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *CallBodyContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitCallBody(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) CallBody() (localctx ICallBodyContext) {
	localctx = NewCallBodyContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 16, RipgenParserRULE_callBody)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(127)
		p.Match(RipgenParserT__9)
	}
	{
		p.SetState(128)
		p.Match(RipgenParserCOL)
	}
	{
		p.SetState(129)
		p.TopTypeSlot()
	}

	return localctx
}

// ICallResponseContext is an interface to support dynamic dispatch.
type ICallResponseContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsCallResponseContext differentiates from other interfaces.
	IsCallResponseContext()
}

type CallResponseContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyCallResponseContext() *CallResponseContext {
	var p = new(CallResponseContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RipgenParserRULE_callResponse
	return p
}

func (*CallResponseContext) IsCallResponseContext() {}

func NewCallResponseContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *CallResponseContext {
	var p = new(CallResponseContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_callResponse

	return p
}

func (s *CallResponseContext) GetParser() antlr.Parser { return s.parser }

func (s *CallResponseContext) COL() antlr.TerminalNode {
	return s.GetToken(RipgenParserCOL, 0)
}

func (s *CallResponseContext) TopTypeSlot() ITopTypeSlotContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ITopTypeSlotContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ITopTypeSlotContext)
}

func (s *CallResponseContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *CallResponseContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *CallResponseContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitCallResponse(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) CallResponse() (localctx ICallResponseContext) {
	localctx = NewCallResponseContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 18, RipgenParserRULE_callResponse)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(131)
		p.Match(RipgenParserT__10)
	}
	{
		p.SetState(132)
		p.Match(RipgenParserCOL)
	}
	{
		p.SetState(133)
		p.TopTypeSlot()
	}

	return localctx
}

// ICallBinaryResponseContext is an interface to support dynamic dispatch.
type ICallBinaryResponseContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsCallBinaryResponseContext differentiates from other interfaces.
	IsCallBinaryResponseContext()
}

type CallBinaryResponseContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyCallBinaryResponseContext() *CallBinaryResponseContext {
	var p = new(CallBinaryResponseContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RipgenParserRULE_callBinaryResponse
	return p
}

func (*CallBinaryResponseContext) IsCallBinaryResponseContext() {}

func NewCallBinaryResponseContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *CallBinaryResponseContext {
	var p = new(CallBinaryResponseContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_callBinaryResponse

	return p
}

func (s *CallBinaryResponseContext) GetParser() antlr.Parser { return s.parser }

func (s *CallBinaryResponseContext) COL() antlr.TerminalNode {
	return s.GetToken(RipgenParserCOL, 0)
}

func (s *CallBinaryResponseContext) MimeType() IMimeTypeContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IMimeTypeContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IMimeTypeContext)
}

func (s *CallBinaryResponseContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *CallBinaryResponseContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *CallBinaryResponseContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitCallBinaryResponse(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) CallBinaryResponse() (localctx ICallBinaryResponseContext) {
	localctx = NewCallBinaryResponseContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 20, RipgenParserRULE_callBinaryResponse)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(135)
		p.Match(RipgenParserT__11)
	}
	{
		p.SetState(136)
		p.Match(RipgenParserCOL)
	}
	{
		p.SetState(137)
		p.MimeType()
	}

	return localctx
}

// ICallStreamResponseContext is an interface to support dynamic dispatch.
type ICallStreamResponseContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsCallStreamResponseContext differentiates from other interfaces.
	IsCallStreamResponseContext()
}

type CallStreamResponseContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyCallStreamResponseContext() *CallStreamResponseContext {
	var p = new(CallStreamResponseContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RipgenParserRULE_callStreamResponse
	return p
}

func (*CallStreamResponseContext) IsCallStreamResponseContext() {}

func NewCallStreamResponseContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *CallStreamResponseContext {
	var p = new(CallStreamResponseContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_callStreamResponse

	return p
}

func (s *CallStreamResponseContext) GetParser() antlr.Parser { return s.parser }

func (s *CallStreamResponseContext) COL() antlr.TerminalNode {
	return s.GetToken(RipgenParserCOL, 0)
}

func (s *CallStreamResponseContext) MimeType() IMimeTypeContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IMimeTypeContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IMimeTypeContext)
}

func (s *CallStreamResponseContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *CallStreamResponseContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *CallStreamResponseContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitCallStreamResponse(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) CallStreamResponse() (localctx ICallStreamResponseContext) {
	localctx = NewCallStreamResponseContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 22, RipgenParserRULE_callStreamResponse)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(139)
		p.Match(RipgenParserT__12)
	}
	{
		p.SetState(140)
		p.Match(RipgenParserCOL)
	}
	{
		p.SetState(141)
		p.MimeType()
	}

	return localctx
}

// IMethodContext is an interface to support dynamic dispatch.
type IMethodContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsMethodContext differentiates from other interfaces.
	IsMethodContext()
}

type MethodContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyMethodContext() *MethodContext {
	var p = new(MethodContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RipgenParserRULE_method
	return p
}

func (*MethodContext) IsMethodContext() {}

func NewMethodContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *MethodContext {
	var p = new(MethodContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_method

	return p
}

func (s *MethodContext) GetParser() antlr.Parser { return s.parser }

func (s *MethodContext) GET() antlr.TerminalNode {
	return s.GetToken(RipgenParserGET, 0)
}

func (s *MethodContext) POST() antlr.TerminalNode {
	return s.GetToken(RipgenParserPOST, 0)
}

func (s *MethodContext) PUT() antlr.TerminalNode {
	return s.GetToken(RipgenParserPUT, 0)
}

func (s *MethodContext) DELETE() antlr.TerminalNode {
	return s.GetToken(RipgenParserDELETE, 0)
}

func (s *MethodContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *MethodContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *MethodContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitMethod(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) Method() (localctx IMethodContext) {
	localctx = NewMethodContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 24, RipgenParserRULE_method)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(143)
		_la = p.GetTokenStream().LA(1)

		if !(((_la)&-(0x1f+1)) == 0 && ((1<<uint(_la))&((1<<RipgenParserGET)|(1<<RipgenParserPOST)|(1<<RipgenParserPUT)|(1<<RipgenParserDELETE))) != 0) {
			p.GetErrorHandler().RecoverInline(p)
		} else {
			p.GetErrorHandler().ReportMatch(p)
			p.Consume()
		}
	}

	return localctx
}

// IUrlParamContext is an interface to support dynamic dispatch.
type IUrlParamContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsUrlParamContext differentiates from other interfaces.
	IsUrlParamContext()
}

type UrlParamContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyUrlParamContext() *UrlParamContext {
	var p = new(UrlParamContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RipgenParserRULE_urlParam
	return p
}

func (*UrlParamContext) IsUrlParamContext() {}

func NewUrlParamContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *UrlParamContext {
	var p = new(UrlParamContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_urlParam

	return p
}

func (s *UrlParamContext) GetParser() antlr.Parser { return s.parser }

func (s *UrlParamContext) NamedType() INamedTypeContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*INamedTypeContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(INamedTypeContext)
}

func (s *UrlParamContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *UrlParamContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *UrlParamContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitUrlParam(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) UrlParam() (localctx IUrlParamContext) {
	localctx = NewUrlParamContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 26, RipgenParserRULE_urlParam)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(145)
		p.Match(RipgenParserT__18)
	}
	{
		p.SetState(146)
		p.NamedType()
	}
	{
		p.SetState(147)
		p.Match(RipgenParserT__19)
	}

	return localctx
}

// IPathPartContext is an interface to support dynamic dispatch.
type IPathPartContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsPathPartContext differentiates from other interfaces.
	IsPathPartContext()
}

type PathPartContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyPathPartContext() *PathPartContext {
	var p = new(PathPartContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RipgenParserRULE_pathPart
	return p
}

func (*PathPartContext) IsPathPartContext() {}

func NewPathPartContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *PathPartContext {
	var p = new(PathPartContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_pathPart

	return p
}

func (s *PathPartContext) GetParser() antlr.Parser { return s.parser }

func (s *PathPartContext) Identifier() IIdentifierContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIdentifierContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *PathPartContext) NamedType() INamedTypeContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*INamedTypeContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(INamedTypeContext)
}

func (s *PathPartContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *PathPartContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *PathPartContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitPathPart(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) PathPart() (localctx IPathPartContext) {
	localctx = NewPathPartContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 28, RipgenParserRULE_pathPart)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	p.SetState(155)
	p.GetErrorHandler().Sync(p)

	switch p.GetTokenStream().LA(1) {
	case RipgenParserT__13:
		{
			p.SetState(149)
			p.Match(RipgenParserT__13)
		}
		{
			p.SetState(150)
			p.Identifier()
		}

	case RipgenParserT__20:
		{
			p.SetState(151)
			p.Match(RipgenParserT__20)
		}
		{
			p.SetState(152)
			p.NamedType()
		}
		{
			p.SetState(153)
			p.Match(RipgenParserT__19)
		}

	default:
		panic(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
	}

	return localctx
}

// INamedTypeContext is an interface to support dynamic dispatch.
type INamedTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsNamedTypeContext differentiates from other interfaces.
	IsNamedTypeContext()
}

type NamedTypeContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyNamedTypeContext() *NamedTypeContext {
	var p = new(NamedTypeContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RipgenParserRULE_namedType
	return p
}

func (*NamedTypeContext) IsNamedTypeContext() {}

func NewNamedTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *NamedTypeContext {
	var p = new(NamedTypeContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_namedType

	return p
}

func (s *NamedTypeContext) GetParser() antlr.Parser { return s.parser }

func (s *NamedTypeContext) PropIdentifier() IPropIdentifierContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IPropIdentifierContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IPropIdentifierContext)
}

func (s *NamedTypeContext) SubTypeSlot() ISubTypeSlotContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ISubTypeSlotContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ISubTypeSlotContext)
}

func (s *NamedTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *NamedTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *NamedTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitNamedType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) NamedType() (localctx INamedTypeContext) {
	localctx = NewNamedTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 30, RipgenParserRULE_namedType)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(157)
		p.PropIdentifier()
	}
	{
		p.SetState(158)
		p.SubTypeSlot()
	}

	return localctx
}

// ITypeDeclarationContext is an interface to support dynamic dispatch.
type ITypeDeclarationContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetName returns the name rule contexts.
	GetName() ITypeIdentifierContext

	// SetName sets the name rule contexts.
	SetName(ITypeIdentifierContext)

	// IsTypeDeclarationContext differentiates from other interfaces.
	IsTypeDeclarationContext()
}

type TypeDeclarationContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
	name   ITypeIdentifierContext
}

func NewEmptyTypeDeclarationContext() *TypeDeclarationContext {
	var p = new(TypeDeclarationContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RipgenParserRULE_typeDeclaration
	return p
}

func (*TypeDeclarationContext) IsTypeDeclarationContext() {}

func NewTypeDeclarationContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *TypeDeclarationContext {
	var p = new(TypeDeclarationContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_typeDeclaration

	return p
}

func (s *TypeDeclarationContext) GetParser() antlr.Parser { return s.parser }

func (s *TypeDeclarationContext) GetName() ITypeIdentifierContext { return s.name }

func (s *TypeDeclarationContext) SetName(v ITypeIdentifierContext) { s.name = v }

func (s *TypeDeclarationContext) TopTypeDef() ITopTypeDefContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ITopTypeDefContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ITopTypeDefContext)
}

func (s *TypeDeclarationContext) TypeIdentifier() ITypeIdentifierContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ITypeIdentifierContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ITypeIdentifierContext)
}

func (s *TypeDeclarationContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *TypeDeclarationContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *TypeDeclarationContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitTypeDeclaration(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) TypeDeclaration() (localctx ITypeDeclarationContext) {
	localctx = NewTypeDeclarationContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 32, RipgenParserRULE_typeDeclaration)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(160)
		p.Match(RipgenParserT__7)
	}
	{
		p.SetState(161)

		var _x = p.TypeIdentifier()

		localctx.(*TypeDeclarationContext).name = _x
	}
	{
		p.SetState(162)
		p.TopTypeDef()
	}

	return localctx
}

// ISubTypeSlotContext is an interface to support dynamic dispatch.
type ISubTypeSlotContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsSubTypeSlotContext differentiates from other interfaces.
	IsSubTypeSlotContext()
}

type SubTypeSlotContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptySubTypeSlotContext() *SubTypeSlotContext {
	var p = new(SubTypeSlotContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RipgenParserRULE_subTypeSlot
	return p
}

func (*SubTypeSlotContext) IsSubTypeSlotContext() {}

func NewSubTypeSlotContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *SubTypeSlotContext {
	var p = new(SubTypeSlotContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_subTypeSlot

	return p
}

func (s *SubTypeSlotContext) GetParser() antlr.Parser { return s.parser }

func (s *SubTypeSlotContext) SubTypeDef() ISubTypeDefContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ISubTypeDefContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ISubTypeDefContext)
}

func (s *SubTypeSlotContext) NULLABLE() antlr.TerminalNode {
	return s.GetToken(RipgenParserNULLABLE, 0)
}

func (s *SubTypeSlotContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *SubTypeSlotContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *SubTypeSlotContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitSubTypeSlot(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) SubTypeSlot() (localctx ISubTypeSlotContext) {
	localctx = NewSubTypeSlotContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 34, RipgenParserRULE_subTypeSlot)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(164)
		p.SubTypeDef()
	}
	p.SetState(166)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	if _la == RipgenParserNULLABLE {
		{
			p.SetState(165)
			p.Match(RipgenParserNULLABLE)
		}

	}

	return localctx
}

// ITopTypeSlotContext is an interface to support dynamic dispatch.
type ITopTypeSlotContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsTopTypeSlotContext differentiates from other interfaces.
	IsTopTypeSlotContext()
}

type TopTypeSlotContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyTopTypeSlotContext() *TopTypeSlotContext {
	var p = new(TopTypeSlotContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RipgenParserRULE_topTypeSlot
	return p
}

func (*TopTypeSlotContext) IsTopTypeSlotContext() {}

func NewTopTypeSlotContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *TopTypeSlotContext {
	var p = new(TopTypeSlotContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_topTypeSlot

	return p
}

func (s *TopTypeSlotContext) GetParser() antlr.Parser { return s.parser }

func (s *TopTypeSlotContext) TopTypeDef() ITopTypeDefContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ITopTypeDefContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ITopTypeDefContext)
}

func (s *TopTypeSlotContext) NULLABLE() antlr.TerminalNode {
	return s.GetToken(RipgenParserNULLABLE, 0)
}

func (s *TopTypeSlotContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *TopTypeSlotContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *TopTypeSlotContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitTopTypeSlot(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) TopTypeSlot() (localctx ITopTypeSlotContext) {
	localctx = NewTopTypeSlotContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 36, RipgenParserRULE_topTypeSlot)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(168)
		p.TopTypeDef()
	}
	p.SetState(170)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	if _la == RipgenParserNULLABLE {
		{
			p.SetState(169)
			p.Match(RipgenParserNULLABLE)
		}

	}

	return localctx
}

// ISubTypeDefContext is an interface to support dynamic dispatch.
type ISubTypeDefContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsSubTypeDefContext differentiates from other interfaces.
	IsSubTypeDefContext()
}

type SubTypeDefContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptySubTypeDefContext() *SubTypeDefContext {
	var p = new(SubTypeDefContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RipgenParserRULE_subTypeDef
	return p
}

func (*SubTypeDefContext) IsSubTypeDefContext() {}

func NewSubTypeDefContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *SubTypeDefContext {
	var p = new(SubTypeDefContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_subTypeDef

	return p
}

func (s *SubTypeDefContext) GetParser() antlr.Parser { return s.parser }

func (s *SubTypeDefContext) IntType() IIntTypeContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IIntTypeContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IIntTypeContext)
}

func (s *SubTypeDefContext) FloatType() IFloatTypeContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IFloatTypeContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IFloatTypeContext)
}

func (s *SubTypeDefContext) StringType() IStringTypeContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IStringTypeContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IStringTypeContext)
}

func (s *SubTypeDefContext) ArrayType() IArrayTypeContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IArrayTypeContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IArrayTypeContext)
}

func (s *SubTypeDefContext) MapType() IMapTypeContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IMapTypeContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IMapTypeContext)
}

func (s *SubTypeDefContext) BoolType() IBoolTypeContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IBoolTypeContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IBoolTypeContext)
}

func (s *SubTypeDefContext) TimeType() ITimeTypeContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ITimeTypeContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ITimeTypeContext)
}

func (s *SubTypeDefContext) AnyType() IAnyTypeContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IAnyTypeContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IAnyTypeContext)
}

func (s *SubTypeDefContext) CustomType() ICustomTypeContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ICustomTypeContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ICustomTypeContext)
}

func (s *SubTypeDefContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *SubTypeDefContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *SubTypeDefContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitSubTypeDef(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) SubTypeDef() (localctx ISubTypeDefContext) {
	localctx = NewSubTypeDefContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 38, RipgenParserRULE_subTypeDef)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.SetState(181)
	p.GetErrorHandler().Sync(p)

	switch p.GetTokenStream().LA(1) {
	case RipgenParserT__0:
		p.EnterOuterAlt(localctx, 1)
		{
			p.SetState(172)
			p.IntType()
		}

	case RipgenParserT__1:
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(173)
			p.FloatType()
		}

	case RipgenParserT__2:
		p.EnterOuterAlt(localctx, 3)
		{
			p.SetState(174)
			p.StringType()
		}

	case RipgenParserT__21:
		p.EnterOuterAlt(localctx, 4)
		{
			p.SetState(175)
			p.ArrayType()
		}

	case RipgenParserT__18:
		p.EnterOuterAlt(localctx, 5)
		{
			p.SetState(176)
			p.MapType()
		}

	case RipgenParserT__4:
		p.EnterOuterAlt(localctx, 6)
		{
			p.SetState(177)
			p.BoolType()
		}

	case RipgenParserT__3:
		p.EnterOuterAlt(localctx, 7)
		{
			p.SetState(178)
			p.TimeType()
		}

	case RipgenParserT__6:
		p.EnterOuterAlt(localctx, 8)
		{
			p.SetState(179)
			p.AnyType()
		}

	case RipgenParserTYPE_IDENTIFIER:
		p.EnterOuterAlt(localctx, 9)
		{
			p.SetState(180)
			p.CustomType()
		}

	default:
		panic(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
	}

	return localctx
}

// ITopTypeDefContext is an interface to support dynamic dispatch.
type ITopTypeDefContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsTopTypeDefContext differentiates from other interfaces.
	IsTopTypeDefContext()
}

type TopTypeDefContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyTopTypeDefContext() *TopTypeDefContext {
	var p = new(TopTypeDefContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RipgenParserRULE_topTypeDef
	return p
}

func (*TopTypeDefContext) IsTopTypeDefContext() {}

func NewTopTypeDefContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *TopTypeDefContext {
	var p = new(TopTypeDefContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_topTypeDef

	return p
}

func (s *TopTypeDefContext) GetParser() antlr.Parser { return s.parser }

func (s *TopTypeDefContext) ObjectType() IObjectTypeContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IObjectTypeContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IObjectTypeContext)
}

func (s *TopTypeDefContext) EnumType() IEnumTypeContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IEnumTypeContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IEnumTypeContext)
}

func (s *TopTypeDefContext) SubTypeDef() ISubTypeDefContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ISubTypeDefContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ISubTypeDefContext)
}

func (s *TopTypeDefContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *TopTypeDefContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *TopTypeDefContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitTopTypeDef(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) TopTypeDef() (localctx ITopTypeDefContext) {
	localctx = NewTopTypeDefContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 40, RipgenParserRULE_topTypeDef)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.SetState(186)
	p.GetErrorHandler().Sync(p)
	switch p.GetInterpreter().AdaptivePredict(p.GetTokenStream(), 13, p.GetParserRuleContext()) {
	case 1:
		p.EnterOuterAlt(localctx, 1)
		{
			p.SetState(183)
			p.ObjectType()
		}

	case 2:
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(184)
			p.EnumType()
		}

	case 3:
		p.EnterOuterAlt(localctx, 3)
		{
			p.SetState(185)
			p.SubTypeDef()
		}

	}

	return localctx
}

// IIntTypeContext is an interface to support dynamic dispatch.
type IIntTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsIntTypeContext differentiates from other interfaces.
	IsIntTypeContext()
}

type IntTypeContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyIntTypeContext() *IntTypeContext {
	var p = new(IntTypeContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RipgenParserRULE_intType
	return p
}

func (*IntTypeContext) IsIntTypeContext() {}

func NewIntTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *IntTypeContext {
	var p = new(IntTypeContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_intType

	return p
}

func (s *IntTypeContext) GetParser() antlr.Parser { return s.parser }
func (s *IntTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IntTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *IntTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitIntType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) IntType() (localctx IIntTypeContext) {
	localctx = NewIntTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 42, RipgenParserRULE_intType)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(188)
		p.Match(RipgenParserT__0)
	}

	return localctx
}

// IFloatTypeContext is an interface to support dynamic dispatch.
type IFloatTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsFloatTypeContext differentiates from other interfaces.
	IsFloatTypeContext()
}

type FloatTypeContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyFloatTypeContext() *FloatTypeContext {
	var p = new(FloatTypeContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RipgenParserRULE_floatType
	return p
}

func (*FloatTypeContext) IsFloatTypeContext() {}

func NewFloatTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *FloatTypeContext {
	var p = new(FloatTypeContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_floatType

	return p
}

func (s *FloatTypeContext) GetParser() antlr.Parser { return s.parser }
func (s *FloatTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *FloatTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *FloatTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitFloatType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) FloatType() (localctx IFloatTypeContext) {
	localctx = NewFloatTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 44, RipgenParserRULE_floatType)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(190)
		p.Match(RipgenParserT__1)
	}

	return localctx
}

// ITimeTypeContext is an interface to support dynamic dispatch.
type ITimeTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsTimeTypeContext differentiates from other interfaces.
	IsTimeTypeContext()
}

type TimeTypeContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyTimeTypeContext() *TimeTypeContext {
	var p = new(TimeTypeContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RipgenParserRULE_timeType
	return p
}

func (*TimeTypeContext) IsTimeTypeContext() {}

func NewTimeTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *TimeTypeContext {
	var p = new(TimeTypeContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_timeType

	return p
}

func (s *TimeTypeContext) GetParser() antlr.Parser { return s.parser }
func (s *TimeTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *TimeTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *TimeTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitTimeType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) TimeType() (localctx ITimeTypeContext) {
	localctx = NewTimeTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 46, RipgenParserRULE_timeType)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(192)
		p.Match(RipgenParserT__3)
	}

	return localctx
}

// IBoolTypeContext is an interface to support dynamic dispatch.
type IBoolTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsBoolTypeContext differentiates from other interfaces.
	IsBoolTypeContext()
}

type BoolTypeContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyBoolTypeContext() *BoolTypeContext {
	var p = new(BoolTypeContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RipgenParserRULE_boolType
	return p
}

func (*BoolTypeContext) IsBoolTypeContext() {}

func NewBoolTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *BoolTypeContext {
	var p = new(BoolTypeContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_boolType

	return p
}

func (s *BoolTypeContext) GetParser() antlr.Parser { return s.parser }
func (s *BoolTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *BoolTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *BoolTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitBoolType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) BoolType() (localctx IBoolTypeContext) {
	localctx = NewBoolTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 48, RipgenParserRULE_boolType)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(194)
		p.Match(RipgenParserT__4)
	}

	return localctx
}

// IStringTypeContext is an interface to support dynamic dispatch.
type IStringTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsStringTypeContext differentiates from other interfaces.
	IsStringTypeContext()
}

type StringTypeContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyStringTypeContext() *StringTypeContext {
	var p = new(StringTypeContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RipgenParserRULE_stringType
	return p
}

func (*StringTypeContext) IsStringTypeContext() {}

func NewStringTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *StringTypeContext {
	var p = new(StringTypeContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_stringType

	return p
}

func (s *StringTypeContext) GetParser() antlr.Parser { return s.parser }
func (s *StringTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *StringTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *StringTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitStringType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) StringType() (localctx IStringTypeContext) {
	localctx = NewStringTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 50, RipgenParserRULE_stringType)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(196)
		p.Match(RipgenParserT__2)
	}

	return localctx
}

// IAnyTypeContext is an interface to support dynamic dispatch.
type IAnyTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsAnyTypeContext differentiates from other interfaces.
	IsAnyTypeContext()
}

type AnyTypeContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyAnyTypeContext() *AnyTypeContext {
	var p = new(AnyTypeContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RipgenParserRULE_anyType
	return p
}

func (*AnyTypeContext) IsAnyTypeContext() {}

func NewAnyTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *AnyTypeContext {
	var p = new(AnyTypeContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_anyType

	return p
}

func (s *AnyTypeContext) GetParser() antlr.Parser { return s.parser }
func (s *AnyTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *AnyTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *AnyTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitAnyType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) AnyType() (localctx IAnyTypeContext) {
	localctx = NewAnyTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 52, RipgenParserRULE_anyType)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(198)
		p.Match(RipgenParserT__6)
	}

	return localctx
}

// IArrayTypeContext is an interface to support dynamic dispatch.
type IArrayTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsArrayTypeContext differentiates from other interfaces.
	IsArrayTypeContext()
}

type ArrayTypeContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyArrayTypeContext() *ArrayTypeContext {
	var p = new(ArrayTypeContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RipgenParserRULE_arrayType
	return p
}

func (*ArrayTypeContext) IsArrayTypeContext() {}

func NewArrayTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ArrayTypeContext {
	var p = new(ArrayTypeContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_arrayType

	return p
}

func (s *ArrayTypeContext) GetParser() antlr.Parser { return s.parser }

func (s *ArrayTypeContext) SubTypeSlot() ISubTypeSlotContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ISubTypeSlotContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ISubTypeSlotContext)
}

func (s *ArrayTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ArrayTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ArrayTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitArrayType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) ArrayType() (localctx IArrayTypeContext) {
	localctx = NewArrayTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 54, RipgenParserRULE_arrayType)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(200)
		p.Match(RipgenParserT__21)
	}
	{
		p.SetState(201)
		p.SubTypeSlot()
	}
	{
		p.SetState(202)
		p.Match(RipgenParserT__22)
	}

	return localctx
}

// IMapTypeContext is an interface to support dynamic dispatch.
type IMapTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsMapTypeContext differentiates from other interfaces.
	IsMapTypeContext()
}

type MapTypeContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyMapTypeContext() *MapTypeContext {
	var p = new(MapTypeContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RipgenParserRULE_mapType
	return p
}

func (*MapTypeContext) IsMapTypeContext() {}

func NewMapTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *MapTypeContext {
	var p = new(MapTypeContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_mapType

	return p
}

func (s *MapTypeContext) GetParser() antlr.Parser { return s.parser }

func (s *MapTypeContext) SubTypeSlot() ISubTypeSlotContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ISubTypeSlotContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ISubTypeSlotContext)
}

func (s *MapTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *MapTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *MapTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitMapType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) MapType() (localctx IMapTypeContext) {
	localctx = NewMapTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 56, RipgenParserRULE_mapType)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(204)
		p.Match(RipgenParserT__18)
	}
	{
		p.SetState(205)
		p.SubTypeSlot()
	}
	{
		p.SetState(206)
		p.Match(RipgenParserT__19)
	}

	return localctx
}

// IObjectTypeContext is an interface to support dynamic dispatch.
type IObjectTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsObjectTypeContext differentiates from other interfaces.
	IsObjectTypeContext()
}

type ObjectTypeContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyObjectTypeContext() *ObjectTypeContext {
	var p = new(ObjectTypeContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RipgenParserRULE_objectType
	return p
}

func (*ObjectTypeContext) IsObjectTypeContext() {}

func NewObjectTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ObjectTypeContext {
	var p = new(ObjectTypeContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_objectType

	return p
}

func (s *ObjectTypeContext) GetParser() antlr.Parser { return s.parser }

func (s *ObjectTypeContext) AllNamedType() []INamedTypeContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*INamedTypeContext)(nil)).Elem())
	var tst = make([]INamedTypeContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(INamedTypeContext)
		}
	}

	return tst
}

func (s *ObjectTypeContext) NamedType(i int) INamedTypeContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*INamedTypeContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(INamedTypeContext)
}

func (s *ObjectTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ObjectTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ObjectTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitObjectType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) ObjectType() (localctx IObjectTypeContext) {
	localctx = NewObjectTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 58, RipgenParserRULE_objectType)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(208)
		p.Match(RipgenParserT__18)
	}
	p.SetState(212)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	for (((_la)&-(0x1f+1)) == 0 && ((1<<uint(_la))&((1<<RipgenParserT__0)|(1<<RipgenParserT__1)|(1<<RipgenParserT__2)|(1<<RipgenParserT__3)|(1<<RipgenParserT__4)|(1<<RipgenParserT__5)|(1<<RipgenParserT__6)|(1<<RipgenParserT__7)|(1<<RipgenParserT__8)|(1<<RipgenParserT__9)|(1<<RipgenParserT__10)|(1<<RipgenParserT__11)|(1<<RipgenParserT__12)|(1<<RipgenParserGET)|(1<<RipgenParserPOST)|(1<<RipgenParserPUT)|(1<<RipgenParserDELETE))) != 0) || _la == RipgenParserPROP_IDENTIFIER {
		{
			p.SetState(209)
			p.NamedType()
		}

		p.SetState(214)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)
	}
	{
		p.SetState(215)
		p.Match(RipgenParserT__19)
	}

	return localctx
}

// IEnumTypeContext is an interface to support dynamic dispatch.
type IEnumTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Get_enumValue returns the _enumValue rule contexts.
	Get_enumValue() IEnumValueContext

	// Set_enumValue sets the _enumValue rule contexts.
	Set_enumValue(IEnumValueContext)

	// GetValues returns the values rule context list.
	GetValues() []IEnumValueContext

	// SetValues sets the values rule context list.
	SetValues([]IEnumValueContext)

	// IsEnumTypeContext differentiates from other interfaces.
	IsEnumTypeContext()
}

type EnumTypeContext struct {
	*antlr.BaseParserRuleContext
	parser     antlr.Parser
	_enumValue IEnumValueContext
	values     []IEnumValueContext
}

func NewEmptyEnumTypeContext() *EnumTypeContext {
	var p = new(EnumTypeContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RipgenParserRULE_enumType
	return p
}

func (*EnumTypeContext) IsEnumTypeContext() {}

func NewEnumTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *EnumTypeContext {
	var p = new(EnumTypeContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_enumType

	return p
}

func (s *EnumTypeContext) GetParser() antlr.Parser { return s.parser }

func (s *EnumTypeContext) Get_enumValue() IEnumValueContext { return s._enumValue }

func (s *EnumTypeContext) Set_enumValue(v IEnumValueContext) { s._enumValue = v }

func (s *EnumTypeContext) GetValues() []IEnumValueContext { return s.values }

func (s *EnumTypeContext) SetValues(v []IEnumValueContext) { s.values = v }

func (s *EnumTypeContext) AllEnumValue() []IEnumValueContext {
	var ts = s.GetTypedRuleContexts(reflect.TypeOf((*IEnumValueContext)(nil)).Elem())
	var tst = make([]IEnumValueContext, len(ts))

	for i, t := range ts {
		if t != nil {
			tst[i] = t.(IEnumValueContext)
		}
	}

	return tst
}

func (s *EnumTypeContext) EnumValue(i int) IEnumValueContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IEnumValueContext)(nil)).Elem(), i)

	if t == nil {
		return nil
	}

	return t.(IEnumValueContext)
}

func (s *EnumTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *EnumTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *EnumTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitEnumType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) EnumType() (localctx IEnumTypeContext) {
	localctx = NewEnumTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 60, RipgenParserRULE_enumType)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(217)
		p.Match(RipgenParserT__5)
	}
	{
		p.SetState(218)
		p.Match(RipgenParserT__23)
	}
	{
		p.SetState(219)

		var _x = p.EnumValue()

		localctx.(*EnumTypeContext)._enumValue = _x
	}
	localctx.(*EnumTypeContext).values = append(localctx.(*EnumTypeContext).values, localctx.(*EnumTypeContext)._enumValue)
	p.SetState(224)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	for _la == RipgenParserT__24 {
		{
			p.SetState(220)
			p.Match(RipgenParserT__24)
		}
		{
			p.SetState(221)

			var _x = p.EnumValue()

			localctx.(*EnumTypeContext)._enumValue = _x
		}
		localctx.(*EnumTypeContext).values = append(localctx.(*EnumTypeContext).values, localctx.(*EnumTypeContext)._enumValue)

		p.SetState(226)
		p.GetErrorHandler().Sync(p)
		_la = p.GetTokenStream().LA(1)
	}
	{
		p.SetState(227)
		p.Match(RipgenParserT__25)
	}

	return localctx
}

// IEnumValueContext is an interface to support dynamic dispatch.
type IEnumValueContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsEnumValueContext differentiates from other interfaces.
	IsEnumValueContext()
}

type EnumValueContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyEnumValueContext() *EnumValueContext {
	var p = new(EnumValueContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RipgenParserRULE_enumValue
	return p
}

func (*EnumValueContext) IsEnumValueContext() {}

func NewEnumValueContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *EnumValueContext {
	var p = new(EnumValueContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_enumValue

	return p
}

func (s *EnumValueContext) GetParser() antlr.Parser { return s.parser }

func (s *EnumValueContext) PropIdentifier() IPropIdentifierContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*IPropIdentifierContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(IPropIdentifierContext)
}

func (s *EnumValueContext) STRING_LITERAL() antlr.TerminalNode {
	return s.GetToken(RipgenParserSTRING_LITERAL, 0)
}

func (s *EnumValueContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *EnumValueContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *EnumValueContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitEnumValue(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) EnumValue() (localctx IEnumValueContext) {
	localctx = NewEnumValueContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 62, RipgenParserRULE_enumValue)
	var _la int

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(229)
		p.PropIdentifier()
	}
	p.SetState(231)
	p.GetErrorHandler().Sync(p)
	_la = p.GetTokenStream().LA(1)

	if _la == RipgenParserSTRING_LITERAL {
		{
			p.SetState(230)
			p.Match(RipgenParserSTRING_LITERAL)
		}

	}

	return localctx
}

// ICustomTypeContext is an interface to support dynamic dispatch.
type ICustomTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// IsCustomTypeContext differentiates from other interfaces.
	IsCustomTypeContext()
}

type CustomTypeContext struct {
	*antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyCustomTypeContext() *CustomTypeContext {
	var p = new(CustomTypeContext)
	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(nil, -1)
	p.RuleIndex = RipgenParserRULE_customType
	return p
}

func (*CustomTypeContext) IsCustomTypeContext() {}

func NewCustomTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *CustomTypeContext {
	var p = new(CustomTypeContext)

	p.BaseParserRuleContext = antlr.NewBaseParserRuleContext(parent, invokingState)

	p.parser = parser
	p.RuleIndex = RipgenParserRULE_customType

	return p
}

func (s *CustomTypeContext) GetParser() antlr.Parser { return s.parser }

func (s *CustomTypeContext) TypeIdentifier() ITypeIdentifierContext {
	var t = s.GetTypedRuleContext(reflect.TypeOf((*ITypeIdentifierContext)(nil)).Elem(), 0)

	if t == nil {
		return nil
	}

	return t.(ITypeIdentifierContext)
}

func (s *CustomTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *CustomTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *CustomTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case RipgenVisitor:
		return t.VisitCustomType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *RipgenParser) CustomType() (localctx ICustomTypeContext) {
	localctx = NewCustomTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 64, RipgenParserRULE_customType)

	defer func() {
		p.ExitRule()
	}()

	defer func() {
		if err := recover(); err != nil {
			if v, ok := err.(antlr.RecognitionException); ok {
				localctx.SetException(v)
				p.GetErrorHandler().ReportError(p, v)
				p.GetErrorHandler().Recover(p, v)
			} else {
				panic(err)
			}
		}
	}()

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(233)
		p.TypeIdentifier()
	}

	return localctx
}
