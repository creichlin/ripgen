// Code generated from parser/Ripgen.g4 by ANTLR 4.8. DO NOT EDIT.

package parser

import (
	"fmt"
	"unicode"

	"github.com/antlr/antlr4/runtime/Go/antlr"
)

// Suppress unused import error
var _ = fmt.Printf
var _ = unicode.IsLetter

var serializedLexerAtn = []uint16{
	3, 24715, 42794, 33075, 47597, 16764, 15335, 30598, 22884, 2, 40, 265,
	8, 1, 4, 2, 9, 2, 4, 3, 9, 3, 4, 4, 9, 4, 4, 5, 9, 5, 4, 6, 9, 6, 4, 7,
	9, 7, 4, 8, 9, 8, 4, 9, 9, 9, 4, 10, 9, 10, 4, 11, 9, 11, 4, 12, 9, 12,
	4, 13, 9, 13, 4, 14, 9, 14, 4, 15, 9, 15, 4, 16, 9, 16, 4, 17, 9, 17, 4,
	18, 9, 18, 4, 19, 9, 19, 4, 20, 9, 20, 4, 21, 9, 21, 4, 22, 9, 22, 4, 23,
	9, 23, 4, 24, 9, 24, 4, 25, 9, 25, 4, 26, 9, 26, 4, 27, 9, 27, 4, 28, 9,
	28, 4, 29, 9, 29, 4, 30, 9, 30, 4, 31, 9, 31, 4, 32, 9, 32, 4, 33, 9, 33,
	4, 34, 9, 34, 4, 35, 9, 35, 4, 36, 9, 36, 4, 37, 9, 37, 4, 38, 9, 38, 4,
	39, 9, 39, 3, 2, 3, 2, 3, 2, 3, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
	3, 4, 3, 4, 3, 4, 3, 4, 3, 4, 3, 4, 3, 4, 3, 5, 3, 5, 3, 5, 3, 5, 3, 5,
	3, 6, 3, 6, 3, 6, 3, 6, 3, 6, 3, 7, 3, 7, 3, 7, 3, 7, 3, 7, 3, 8, 3, 8,
	3, 8, 3, 8, 3, 9, 3, 9, 3, 9, 3, 9, 3, 9, 3, 10, 3, 10, 3, 10, 3, 10, 3,
	10, 3, 11, 3, 11, 3, 11, 3, 11, 3, 11, 3, 12, 3, 12, 3, 12, 3, 12, 3, 12,
	3, 12, 3, 12, 3, 12, 3, 12, 3, 13, 3, 13, 3, 13, 3, 13, 3, 13, 3, 13, 3,
	13, 3, 13, 3, 13, 3, 13, 3, 13, 3, 13, 3, 13, 3, 13, 3, 13, 3, 14, 3, 14,
	3, 14, 3, 14, 3, 14, 3, 14, 3, 14, 3, 14, 3, 14, 3, 14, 3, 14, 3, 14, 3,
	14, 3, 14, 3, 14, 3, 15, 3, 15, 3, 16, 3, 16, 3, 17, 3, 17, 3, 18, 3, 18,
	3, 19, 3, 19, 3, 20, 3, 20, 3, 21, 3, 21, 3, 22, 3, 22, 3, 22, 3, 23, 3,
	23, 3, 24, 3, 24, 3, 25, 3, 25, 3, 26, 3, 26, 3, 27, 3, 27, 3, 28, 3, 28,
	3, 28, 3, 28, 3, 29, 3, 29, 3, 29, 3, 29, 3, 29, 3, 30, 3, 30, 3, 30, 3,
	30, 3, 31, 3, 31, 3, 31, 3, 31, 3, 31, 3, 31, 3, 31, 3, 32, 3, 32, 3, 33,
	3, 33, 3, 34, 3, 34, 3, 35, 3, 35, 3, 35, 3, 35, 7, 35, 227, 10, 35, 12,
	35, 14, 35, 230, 11, 35, 3, 35, 3, 35, 3, 36, 3, 36, 7, 36, 236, 10, 36,
	12, 36, 14, 36, 239, 11, 36, 3, 37, 3, 37, 7, 37, 243, 10, 37, 12, 37,
	14, 37, 246, 11, 37, 3, 38, 3, 38, 3, 38, 3, 38, 7, 38, 252, 10, 38, 12,
	38, 14, 38, 255, 11, 38, 3, 38, 3, 38, 3, 39, 6, 39, 260, 10, 39, 13, 39,
	14, 39, 261, 3, 39, 3, 39, 2, 2, 40, 3, 3, 5, 4, 7, 5, 9, 6, 11, 7, 13,
	8, 15, 9, 17, 10, 19, 11, 21, 12, 23, 13, 25, 14, 27, 15, 29, 16, 31, 17,
	33, 18, 35, 19, 37, 20, 39, 21, 41, 22, 43, 23, 45, 24, 47, 25, 49, 26,
	51, 27, 53, 28, 55, 29, 57, 30, 59, 31, 61, 32, 63, 33, 65, 34, 67, 35,
	69, 36, 71, 37, 73, 38, 75, 39, 77, 40, 3, 2, 8, 4, 2, 12, 12, 15, 15,
	3, 2, 67, 92, 5, 2, 50, 59, 67, 92, 99, 124, 3, 2, 99, 124, 5, 2, 12, 12,
	15, 15, 36, 36, 5, 2, 11, 12, 15, 15, 34, 34, 2, 270, 2, 3, 3, 2, 2, 2,
	2, 5, 3, 2, 2, 2, 2, 7, 3, 2, 2, 2, 2, 9, 3, 2, 2, 2, 2, 11, 3, 2, 2, 2,
	2, 13, 3, 2, 2, 2, 2, 15, 3, 2, 2, 2, 2, 17, 3, 2, 2, 2, 2, 19, 3, 2, 2,
	2, 2, 21, 3, 2, 2, 2, 2, 23, 3, 2, 2, 2, 2, 25, 3, 2, 2, 2, 2, 27, 3, 2,
	2, 2, 2, 29, 3, 2, 2, 2, 2, 31, 3, 2, 2, 2, 2, 33, 3, 2, 2, 2, 2, 35, 3,
	2, 2, 2, 2, 37, 3, 2, 2, 2, 2, 39, 3, 2, 2, 2, 2, 41, 3, 2, 2, 2, 2, 43,
	3, 2, 2, 2, 2, 45, 3, 2, 2, 2, 2, 47, 3, 2, 2, 2, 2, 49, 3, 2, 2, 2, 2,
	51, 3, 2, 2, 2, 2, 53, 3, 2, 2, 2, 2, 55, 3, 2, 2, 2, 2, 57, 3, 2, 2, 2,
	2, 59, 3, 2, 2, 2, 2, 61, 3, 2, 2, 2, 2, 63, 3, 2, 2, 2, 2, 65, 3, 2, 2,
	2, 2, 67, 3, 2, 2, 2, 2, 69, 3, 2, 2, 2, 2, 71, 3, 2, 2, 2, 2, 73, 3, 2,
	2, 2, 2, 75, 3, 2, 2, 2, 2, 77, 3, 2, 2, 2, 3, 79, 3, 2, 2, 2, 5, 83, 3,
	2, 2, 2, 7, 89, 3, 2, 2, 2, 9, 96, 3, 2, 2, 2, 11, 101, 3, 2, 2, 2, 13,
	106, 3, 2, 2, 2, 15, 111, 3, 2, 2, 2, 17, 115, 3, 2, 2, 2, 19, 120, 3,
	2, 2, 2, 21, 125, 3, 2, 2, 2, 23, 130, 3, 2, 2, 2, 25, 139, 3, 2, 2, 2,
	27, 154, 3, 2, 2, 2, 29, 169, 3, 2, 2, 2, 31, 171, 3, 2, 2, 2, 33, 173,
	3, 2, 2, 2, 35, 175, 3, 2, 2, 2, 37, 177, 3, 2, 2, 2, 39, 179, 3, 2, 2,
	2, 41, 181, 3, 2, 2, 2, 43, 183, 3, 2, 2, 2, 45, 186, 3, 2, 2, 2, 47, 188,
	3, 2, 2, 2, 49, 190, 3, 2, 2, 2, 51, 192, 3, 2, 2, 2, 53, 194, 3, 2, 2,
	2, 55, 196, 3, 2, 2, 2, 57, 200, 3, 2, 2, 2, 59, 205, 3, 2, 2, 2, 61, 209,
	3, 2, 2, 2, 63, 216, 3, 2, 2, 2, 65, 218, 3, 2, 2, 2, 67, 220, 3, 2, 2,
	2, 69, 222, 3, 2, 2, 2, 71, 233, 3, 2, 2, 2, 73, 240, 3, 2, 2, 2, 75, 247,
	3, 2, 2, 2, 77, 259, 3, 2, 2, 2, 79, 80, 7, 107, 2, 2, 80, 81, 7, 112,
	2, 2, 81, 82, 7, 118, 2, 2, 82, 4, 3, 2, 2, 2, 83, 84, 7, 104, 2, 2, 84,
	85, 7, 110, 2, 2, 85, 86, 7, 113, 2, 2, 86, 87, 7, 99, 2, 2, 87, 88, 7,
	118, 2, 2, 88, 6, 3, 2, 2, 2, 89, 90, 7, 117, 2, 2, 90, 91, 7, 118, 2,
	2, 91, 92, 7, 116, 2, 2, 92, 93, 7, 107, 2, 2, 93, 94, 7, 112, 2, 2, 94,
	95, 7, 105, 2, 2, 95, 8, 3, 2, 2, 2, 96, 97, 7, 118, 2, 2, 97, 98, 7, 107,
	2, 2, 98, 99, 7, 111, 2, 2, 99, 100, 7, 103, 2, 2, 100, 10, 3, 2, 2, 2,
	101, 102, 7, 100, 2, 2, 102, 103, 7, 113, 2, 2, 103, 104, 7, 113, 2, 2,
	104, 105, 7, 110, 2, 2, 105, 12, 3, 2, 2, 2, 106, 107, 7, 103, 2, 2, 107,
	108, 7, 112, 2, 2, 108, 109, 7, 119, 2, 2, 109, 110, 7, 111, 2, 2, 110,
	14, 3, 2, 2, 2, 111, 112, 7, 99, 2, 2, 112, 113, 7, 112, 2, 2, 113, 114,
	7, 123, 2, 2, 114, 16, 3, 2, 2, 2, 115, 116, 7, 118, 2, 2, 116, 117, 7,
	123, 2, 2, 117, 118, 7, 114, 2, 2, 118, 119, 7, 103, 2, 2, 119, 18, 3,
	2, 2, 2, 120, 121, 7, 112, 2, 2, 121, 122, 7, 99, 2, 2, 122, 123, 7, 111,
	2, 2, 123, 124, 7, 103, 2, 2, 124, 20, 3, 2, 2, 2, 125, 126, 7, 100, 2,
	2, 126, 127, 7, 113, 2, 2, 127, 128, 7, 102, 2, 2, 128, 129, 7, 123, 2,
	2, 129, 22, 3, 2, 2, 2, 130, 131, 7, 116, 2, 2, 131, 132, 7, 103, 2, 2,
	132, 133, 7, 117, 2, 2, 133, 134, 7, 114, 2, 2, 134, 135, 7, 113, 2, 2,
	135, 136, 7, 112, 2, 2, 136, 137, 7, 117, 2, 2, 137, 138, 7, 103, 2, 2,
	138, 24, 3, 2, 2, 2, 139, 140, 7, 100, 2, 2, 140, 141, 7, 107, 2, 2, 141,
	142, 7, 112, 2, 2, 142, 143, 7, 99, 2, 2, 143, 144, 7, 116, 2, 2, 144,
	145, 7, 123, 2, 2, 145, 146, 7, 84, 2, 2, 146, 147, 7, 103, 2, 2, 147,
	148, 7, 117, 2, 2, 148, 149, 7, 114, 2, 2, 149, 150, 7, 113, 2, 2, 150,
	151, 7, 112, 2, 2, 151, 152, 7, 117, 2, 2, 152, 153, 7, 103, 2, 2, 153,
	26, 3, 2, 2, 2, 154, 155, 7, 117, 2, 2, 155, 156, 7, 118, 2, 2, 156, 157,
	7, 116, 2, 2, 157, 158, 7, 103, 2, 2, 158, 159, 7, 99, 2, 2, 159, 160,
	7, 111, 2, 2, 160, 161, 7, 84, 2, 2, 161, 162, 7, 103, 2, 2, 162, 163,
	7, 117, 2, 2, 163, 164, 7, 114, 2, 2, 164, 165, 7, 113, 2, 2, 165, 166,
	7, 112, 2, 2, 166, 167, 7, 117, 2, 2, 167, 168, 7, 103, 2, 2, 168, 28,
	3, 2, 2, 2, 169, 170, 7, 49, 2, 2, 170, 30, 3, 2, 2, 2, 171, 172, 7, 45,
	2, 2, 172, 32, 3, 2, 2, 2, 173, 174, 7, 47, 2, 2, 174, 34, 3, 2, 2, 2,
	175, 176, 7, 48, 2, 2, 176, 36, 3, 2, 2, 2, 177, 178, 7, 40, 2, 2, 178,
	38, 3, 2, 2, 2, 179, 180, 7, 125, 2, 2, 180, 40, 3, 2, 2, 2, 181, 182,
	7, 127, 2, 2, 182, 42, 3, 2, 2, 2, 183, 184, 7, 49, 2, 2, 184, 185, 7,
	125, 2, 2, 185, 44, 3, 2, 2, 2, 186, 187, 7, 93, 2, 2, 187, 46, 3, 2, 2,
	2, 188, 189, 7, 95, 2, 2, 189, 48, 3, 2, 2, 2, 190, 191, 7, 42, 2, 2, 191,
	50, 3, 2, 2, 2, 192, 193, 7, 46, 2, 2, 193, 52, 3, 2, 2, 2, 194, 195, 7,
	43, 2, 2, 195, 54, 3, 2, 2, 2, 196, 197, 7, 105, 2, 2, 197, 198, 7, 103,
	2, 2, 198, 199, 7, 118, 2, 2, 199, 56, 3, 2, 2, 2, 200, 201, 7, 114, 2,
	2, 201, 202, 7, 113, 2, 2, 202, 203, 7, 117, 2, 2, 203, 204, 7, 118, 2,
	2, 204, 58, 3, 2, 2, 2, 205, 206, 7, 114, 2, 2, 206, 207, 7, 119, 2, 2,
	207, 208, 7, 118, 2, 2, 208, 60, 3, 2, 2, 2, 209, 210, 7, 102, 2, 2, 210,
	211, 7, 103, 2, 2, 211, 212, 7, 110, 2, 2, 212, 213, 7, 103, 2, 2, 213,
	214, 7, 118, 2, 2, 214, 215, 7, 103, 2, 2, 215, 62, 3, 2, 2, 2, 216, 217,
	7, 60, 2, 2, 217, 64, 3, 2, 2, 2, 218, 219, 7, 44, 2, 2, 219, 66, 3, 2,
	2, 2, 220, 221, 7, 65, 2, 2, 221, 68, 3, 2, 2, 2, 222, 223, 7, 49, 2, 2,
	223, 224, 7, 49, 2, 2, 224, 228, 3, 2, 2, 2, 225, 227, 10, 2, 2, 2, 226,
	225, 3, 2, 2, 2, 227, 230, 3, 2, 2, 2, 228, 226, 3, 2, 2, 2, 228, 229,
	3, 2, 2, 2, 229, 231, 3, 2, 2, 2, 230, 228, 3, 2, 2, 2, 231, 232, 8, 35,
	2, 2, 232, 70, 3, 2, 2, 2, 233, 237, 9, 3, 2, 2, 234, 236, 9, 4, 2, 2,
	235, 234, 3, 2, 2, 2, 236, 239, 3, 2, 2, 2, 237, 235, 3, 2, 2, 2, 237,
	238, 3, 2, 2, 2, 238, 72, 3, 2, 2, 2, 239, 237, 3, 2, 2, 2, 240, 244, 9,
	5, 2, 2, 241, 243, 9, 4, 2, 2, 242, 241, 3, 2, 2, 2, 243, 246, 3, 2, 2,
	2, 244, 242, 3, 2, 2, 2, 244, 245, 3, 2, 2, 2, 245, 74, 3, 2, 2, 2, 246,
	244, 3, 2, 2, 2, 247, 253, 7, 36, 2, 2, 248, 252, 10, 6, 2, 2, 249, 250,
	7, 94, 2, 2, 250, 252, 7, 36, 2, 2, 251, 248, 3, 2, 2, 2, 251, 249, 3,
	2, 2, 2, 252, 255, 3, 2, 2, 2, 253, 251, 3, 2, 2, 2, 253, 254, 3, 2, 2,
	2, 254, 256, 3, 2, 2, 2, 255, 253, 3, 2, 2, 2, 256, 257, 7, 36, 2, 2, 257,
	76, 3, 2, 2, 2, 258, 260, 9, 7, 2, 2, 259, 258, 3, 2, 2, 2, 260, 261, 3,
	2, 2, 2, 261, 259, 3, 2, 2, 2, 261, 262, 3, 2, 2, 2, 262, 263, 3, 2, 2,
	2, 263, 264, 8, 39, 2, 2, 264, 78, 3, 2, 2, 2, 9, 2, 228, 237, 244, 251,
	253, 261, 3, 8, 2, 2,
}

var lexerDeserializer = antlr.NewATNDeserializer(nil)
var lexerAtn = lexerDeserializer.DeserializeFromUInt16(serializedLexerAtn)

var lexerChannelNames = []string{
	"DEFAULT_TOKEN_CHANNEL", "HIDDEN",
}

var lexerModeNames = []string{
	"DEFAULT_MODE",
}

var lexerLiteralNames = []string{
	"", "'int'", "'float'", "'string'", "'time'", "'bool'", "'enum'", "'any'",
	"'type'", "'name'", "'body'", "'response'", "'binaryResponse'", "'streamResponse'",
	"'/'", "'+'", "'-'", "'.'", "'&'", "'{'", "'}'", "'/{'", "'['", "']'",
	"'('", "','", "')'", "'get'", "'post'", "'put'", "'delete'", "':'", "'*'",
	"'?'",
}

var lexerSymbolicNames = []string{
	"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
	"", "", "", "", "", "", "", "", "", "GET", "POST", "PUT", "DELETE", "COL",
	"STAR", "NULLABLE", "Comment", "TYPE_IDENTIFIER", "PROP_IDENTIFIER", "STRING_LITERAL",
	"WHITESPACE",
}

var lexerRuleNames = []string{
	"T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", "T__7", "T__8",
	"T__9", "T__10", "T__11", "T__12", "T__13", "T__14", "T__15", "T__16",
	"T__17", "T__18", "T__19", "T__20", "T__21", "T__22", "T__23", "T__24",
	"T__25", "GET", "POST", "PUT", "DELETE", "COL", "STAR", "NULLABLE", "Comment",
	"TYPE_IDENTIFIER", "PROP_IDENTIFIER", "STRING_LITERAL", "WHITESPACE",
}

type RipgenLexer struct {
	*antlr.BaseLexer
	channelNames []string
	modeNames    []string
	// TODO: EOF string
}

var lexerDecisionToDFA = make([]*antlr.DFA, len(lexerAtn.DecisionToState))

func init() {
	for index, ds := range lexerAtn.DecisionToState {
		lexerDecisionToDFA[index] = antlr.NewDFA(ds, index)
	}
}

func NewRipgenLexer(input antlr.CharStream) *RipgenLexer {

	l := new(RipgenLexer)

	l.BaseLexer = antlr.NewBaseLexer(input)
	l.Interpreter = antlr.NewLexerATNSimulator(l, lexerAtn, lexerDecisionToDFA, antlr.NewPredictionContextCache())

	l.channelNames = lexerChannelNames
	l.modeNames = lexerModeNames
	l.RuleNames = lexerRuleNames
	l.LiteralNames = lexerLiteralNames
	l.SymbolicNames = lexerSymbolicNames
	l.GrammarFileName = "Ripgen.g4"
	// TODO: l.EOF = antlr.TokenEOF

	return l
}

// RipgenLexer tokens.
const (
	RipgenLexerT__0            = 1
	RipgenLexerT__1            = 2
	RipgenLexerT__2            = 3
	RipgenLexerT__3            = 4
	RipgenLexerT__4            = 5
	RipgenLexerT__5            = 6
	RipgenLexerT__6            = 7
	RipgenLexerT__7            = 8
	RipgenLexerT__8            = 9
	RipgenLexerT__9            = 10
	RipgenLexerT__10           = 11
	RipgenLexerT__11           = 12
	RipgenLexerT__12           = 13
	RipgenLexerT__13           = 14
	RipgenLexerT__14           = 15
	RipgenLexerT__15           = 16
	RipgenLexerT__16           = 17
	RipgenLexerT__17           = 18
	RipgenLexerT__18           = 19
	RipgenLexerT__19           = 20
	RipgenLexerT__20           = 21
	RipgenLexerT__21           = 22
	RipgenLexerT__22           = 23
	RipgenLexerT__23           = 24
	RipgenLexerT__24           = 25
	RipgenLexerT__25           = 26
	RipgenLexerGET             = 27
	RipgenLexerPOST            = 28
	RipgenLexerPUT             = 29
	RipgenLexerDELETE          = 30
	RipgenLexerCOL             = 31
	RipgenLexerSTAR            = 32
	RipgenLexerNULLABLE        = 33
	RipgenLexerComment         = 34
	RipgenLexerTYPE_IDENTIFIER = 35
	RipgenLexerPROP_IDENTIFIER = 36
	RipgenLexerSTRING_LITERAL  = 37
	RipgenLexerWHITESPACE      = 38
)
