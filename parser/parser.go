package parser

import (
	"fmt"

	"github.com/antlr/antlr4/runtime/Go/antlr"
	"gitlab.com/creichlin/ripgen/ast"
)

func Parse(i string) (*ast.AST, error) {
	input := antlr.NewInputStream(i)
	lexer := NewRipgenLexer(input)
	stream := antlr.NewCommonTokenStream(lexer, 0)

	p := NewRipgenParser(stream)
	p.AddErrorListener(NewErrorListener())
	p.BuildParseTrees = true
	tree := p.Start()

	visi := &ASTVisitor{
		BaseRipgenVisitor: &BaseRipgenVisitor{},
	}

	result := tree.Accept(visi).(*ast.AST)

	err := normalize(result)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func normalize(a *ast.AST) error {
	// first we add type declarations for body and response when the are
	// of type enum or struct
	for _, c := range a.Calls {
		c.Body = makeDeclaration(a, c.Name+"Body", c.Body)
		if js, isj := c.Response.(*ast.JSONIO); isj {
			js.Type = makeDeclaration(a, c.Name+"Response", js.Type)
		}
	}

	var err error
	for _, ct := range a.TypeDeclarations {
		ct.Type, err = resolveType(a, ct.Type)
	}

	for _, c := range a.Calls {
		for _, p := range c.AllParameters() {
			p.Type, err = resolveType(a, p.Type)
			if err != nil {
				return err
			}
		}

		c.Body, err = resolveType(a, c.Body)
		if err != nil {
			return err
		}

		resp, isType := c.Response.(*ast.JSONIO)
		if isType {
			resp.Type, err = resolveType(a, resp.Type)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func makeDeclaration(a *ast.AST, name string, in ast.Type) ast.Type {
	if enum, is := in.(*ast.EnumType); is {
		a.TypeDeclarations = append(a.TypeDeclarations, &ast.TypeDeclaration{
			Name: name,
			Type: enum,
		})
		return &ast.CustomType{
			Name: name,
		}
	}

	if obj, is := in.(*ast.ObjectType); is {
		a.TypeDeclarations = append(a.TypeDeclarations, &ast.TypeDeclaration{
			Name: name,
			Type: obj,
		})
		return &ast.CustomType{
			Name: name,
		}
	}

	if obj, is := in.(*ast.NullableType); is {
		obj.Type = makeDeclaration(a, name, obj.Type)
		return obj
	}
	return in
}

func resolveType(a *ast.AST, t ast.Type) (ast.Type, error) {
	switch tt := t.(type) {
	case *ast.CustomType:
		ct := a.GetTypeDeclaration(tt.Name)
		if ct == nil {
			return nil, fmt.Errorf("Could not find type '%v'", tt.Name)
		}
		tt.Type = ct.Type
	case *ast.ArrayType:
		var err error
		tt.Type, err = resolveType(a, tt.Type)
		if err != nil {
			return nil, err
		}
	case *ast.MapType:
		var err error
		tt.Type, err = resolveType(a, tt.Type)
		if err != nil {
			return nil, err
		}
	case *ast.ObjectType:
		var err error
		for _, f := range tt.Fields {
			f.Type, err = resolveType(a, f.Type)
			if err != nil {
				return nil, err
			}
		}
	case *ast.NullableType:
		var err error
		tt.Type, err = resolveType(a, tt.Type)
		if err != nil {
			return nil, err
		}
	}
	return t, nil
}
