grammar Ripgen;

GET: 'get';
POST: 'post';
PUT: 'put';
DELETE: 'delete';

COL: ':';
STAR: '*';
NULLABLE: '?';

Comment: '//' ~( '\r' | '\n')* -> skip;

TYPE_IDENTIFIER: [A-Z][a-zA-Z0-9]*;
PROP_IDENTIFIER: [a-z][a-zA-Z0-9]*;

STRING_LITERAL: '"' (~["\r\n] | '\\"')* '"';

WHITESPACE: [ \r\n\t]+ -> skip;

// Rules
start: root EOF;

typeIdentifier: TYPE_IDENTIFIER;

propIdentifier:
	PROP_IDENTIFIER
	| 'int'
	| 'float'
	| 'string'
	| 'time'
	| 'bool'
	| 'enum'
	| 'any'
	| GET
	| POST
	| PUT
	| DELETE
	| 'type'
	| 'name'
	| 'body'
	| 'response'
	| 'binaryResponse'
	| 'streamResponse';

identifier: typeIdentifier | propIdentifier;

mimeType:
	identifier '/' identifier (('+' | '-' | '.') identifier)*;

root: (typeDeclaration | call)*;

call:
	method (pathPart)+ ('?' (urlParam) ('&' urlParam)*)? '{' callName callBody? (
		callResponse
		| callBinaryResponse
		| callStreamResponse
	)? '}';

callName: 'name' COL propIdentifier;
callBody: 'body' COL topTypeSlot;
callResponse: 'response' COL topTypeSlot;
callBinaryResponse: 'binaryResponse' COL mimeType;
callStreamResponse: 'streamResponse' COL mimeType;

method: (GET | POST | PUT | DELETE);

urlParam: '{' namedType '}';

pathPart: ('/' identifier | '/{' namedType '}');

namedType: propIdentifier subTypeSlot;

typeDeclaration: 'type' name = typeIdentifier topTypeDef;

subTypeSlot: subTypeDef NULLABLE?;

topTypeSlot: topTypeDef NULLABLE?;

subTypeDef:
	intType
	| floatType
	| stringType
	| arrayType
	| mapType
	| boolType
	| timeType
	| anyType
	| customType;

topTypeDef: objectType | enumType | subTypeDef;

intType: 'int';

floatType: 'float';

timeType: 'time';

boolType: 'bool';

stringType: 'string';

anyType: 'any';

arrayType: '[' subTypeSlot ']';

mapType: '{' subTypeSlot '}';

objectType: '{' namedType* '}';

enumType:
	'enum' '(' values += enumValue (',' values += enumValue)* ')';

enumValue: propIdentifier STRING_LITERAL?;

customType: typeIdentifier;