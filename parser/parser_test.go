package parser

import "testing"

func TestParseIndent(t *testing.T) {
	Parse(`
	interfacePackage: calls
	handlerPackage: httph
	contextType: *Context



	get /foo/bar/{token string}/{id int}?{count int}&{order int} {
		name: getFooByID
		body: {
			type enum (foo, bar, baz)
			size int
			avg float
			counts [ int ]
			items [{
				desc string
			}]
		}
		response: int
	}`)
}
