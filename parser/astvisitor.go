package parser

import (
	"fmt"

	"gitlab.com/creichlin/ripgen/ast"
)

type ASTVisitor struct {
	*BaseRipgenVisitor
}

func (v *ASTVisitor) VisitStart(ctx *StartContext) interface{} {
	return ctx.Root().Accept(v)
}

func (v *ASTVisitor) VisitRoot(ctx *RootContext) interface{} {
	a := &ast.AST{}

	for _, defDec := range ctx.AllTypeDeclaration() {
		c := defDec.Accept(v)
		a.TypeDeclarations = append(a.TypeDeclarations, c.(*ast.TypeDeclaration))
	}

	for _, call := range ctx.AllCall() {
		c := call.Accept(v)
		a.Calls = append(a.Calls, c.(*ast.Call))
	}

	return a
}

func (v *ASTVisitor) VisitCall(ctx *CallContext) interface{} {
	name := ctx.CallName().(*CallNameContext).PropIdentifier().GetText()
	call := &ast.Call{
		Method: ctx.Method().GetText(),
		Name:   name,
	}

	if ctx.CallBody() != nil {
		call.Body = ctx.CallBody().Accept(v).(ast.Type)
	}

	if ctx.CallResponse() != nil {
		call.Response = ctx.CallResponse().Accept(v).(ast.IO)
	}

	if ctx.CallBinaryResponse() != nil {
		call.Response = ctx.CallBinaryResponse().Accept(v).(ast.IO)
	}

	if ctx.CallStreamResponse() != nil {
		call.Response = ctx.CallStreamResponse().Accept(v).(ast.IO)
	}

	for _, pp := range ctx.AllPathPart() {
		call.URL = append(call.URL, pp.Accept(v).(ast.URLPart))
	}

	for _, up := range ctx.AllUrlParam() {
		call.Parameters = append(call.Parameters, (up.(*UrlParamContext).NamedType()).Accept(v).(*ast.NamedType))
	}

	return call
}

func (v *ASTVisitor) VisitCallBody(ctx *CallBodyContext) interface{} {
	def := ctx.TopTypeSlot().Accept(v)
	return def
}

func (v *ASTVisitor) VisitCallResponse(ctx *CallResponseContext) interface{} {
	return &ast.JSONIO{Type: ctx.TopTypeSlot().Accept(v).(ast.Type)}
}

func (v *ASTVisitor) VisitCallBinaryResponse(ctx *CallBinaryResponseContext) interface{} {
	return &ast.BinaryIO{MimeType: ctx.MimeType().GetText()}
}

func (v *ASTVisitor) VisitCallStreamResponse(ctx *CallStreamResponseContext) interface{} {
	return &ast.StreamIO{MimeType: ctx.MimeType().GetText()}
}

func (v *ASTVisitor) VisitPathPart(ctx *PathPartContext) interface{} {

	id := ctx.Identifier()
	if id != nil {
		return ast.StaticURLPart(id.GetText())
	}

	nt := ctx.NamedType().Accept(v)

	return &ast.ParameterURLPart{
		Parameter: nt.(*ast.NamedType),
	}
}

func (v *ASTVisitor) VisitNamedType(ctx *NamedTypeContext) interface{} {
	t := ctx.SubTypeSlot().Accept(v)
	name := ctx.PropIdentifier().GetText()
	return &ast.NamedType{
		Name: name,
		Type: t.(ast.Type),
	}
}

func (v *ASTVisitor) VisitSubTypeDef(ctx *SubTypeDefContext) interface{} {
	if ctx.IntType() != nil {
		return &ast.IntType{}
	}
	if ctx.FloatType() != nil {
		return &ast.FloatType{}
	}
	if ctx.StringType() != nil {
		return &ast.StringType{}
	}
	if ctx.TimeType() != nil {
		return &ast.TimeType{}
	}
	if ctx.BoolType() != nil {
		return &ast.BoolType{}
	}
	if ctx.AnyType() != nil {
		return &ast.AnyType{}
	}
	if ctx.ArrayType() != nil {
		at := ctx.ArrayType().Accept(v).(*ast.ArrayType)
		return at
	}
	if ctx.MapType() != nil {
		mt := ctx.MapType().Accept(v).(*ast.MapType)
		return mt
	}
	if ctx.CustomType() != nil {
		ct := ctx.CustomType().Accept(v).(*ast.CustomType)
		return ct
	}

	panic(fmt.Sprintf("unknown type %+v", ctx))
}

func (v *ASTVisitor) VisitTopTypeSlot(ctx *TopTypeSlotContext) interface{} {
	if ctx.NULLABLE() != nil {
		return &ast.NullableType{
			Type: ctx.TopTypeDef().Accept(v).(ast.Type),
		}
	}
	return ctx.TopTypeDef().Accept(v)
}

func (v *ASTVisitor) VisitSubTypeSlot(ctx *SubTypeSlotContext) interface{} {
	if ctx.NULLABLE() != nil {
		return &ast.NullableType{
			Type: ctx.SubTypeDef().Accept(v).(ast.Type),
		}
	}
	return ctx.SubTypeDef().Accept(v)
}

func (v *ASTVisitor) VisitTopTypeDef(ctx *TopTypeDefContext) interface{} {
	if ctx.ObjectType() != nil {
		ot := ctx.ObjectType().Accept(v).(*ast.ObjectType)
		return ot
	}
	if ctx.EnumType() != nil {
		ct := ctx.EnumType().Accept(v).(*ast.EnumType)
		return ct
	}
	if ctx.SubTypeDef() != nil {
		return ctx.SubTypeDef().Accept(v)
	}

	panic(fmt.Sprintf("unknown type %+v", ctx))
}

func (v *ASTVisitor) VisitObjectType(ctx *ObjectTypeContext) interface{} {
	ot := &ast.ObjectType{}
	for _, nt := range ctx.AllNamedType() {
		ot.Fields = append(ot.Fields, nt.Accept(v).(*ast.NamedType))
	}
	return ot
}

func (v *ASTVisitor) VisitMapType(ctx *MapTypeContext) interface{} {
	return &ast.MapType{
		Type: ctx.SubTypeSlot().Accept(v).(ast.Type),
	}
}

func (v *ASTVisitor) VisitCustomType(ctx *CustomTypeContext) interface{} {
	name := ctx.TypeIdentifier().GetText()
	return &ast.CustomType{Name: name}
}

func (v *ASTVisitor) VisitArrayType(ctx *ArrayTypeContext) interface{} {
	return &ast.ArrayType{
		Type: ctx.SubTypeSlot().Accept(v).(ast.Type),
	}
}

func (v *ASTVisitor) VisitEnumType(ctx *EnumTypeContext) interface{} {
	vals := []ast.EnumValue{}
	for _, ev := range ctx.GetValues() {
		vals = append(vals, ev.Accept(v).(ast.EnumValue))
	}

	return &ast.EnumType{
		Values: vals,
	}
}
func (v *ASTVisitor) VisitEnumValue(ctx *EnumValueContext) interface{} {
	val := ast.EnumValue{
		Name:  ctx.PropIdentifier().GetText(),
		Value: ctx.PropIdentifier().GetText(),
	}

	if ctx.STRING_LITERAL() != nil {
		str := ctx.STRING_LITERAL().GetText()
		val.Value = str[1 : len(str)-1]
	}
	return val
}

func (v *ASTVisitor) VisitTypeDeclaration(ctx *TypeDeclarationContext) interface{} {
	name := ctx.GetName().GetText()
	return &ast.TypeDeclaration{
		Name: name,
		Type: ctx.TopTypeDef().Accept(v).(ast.Type),
	}
}
