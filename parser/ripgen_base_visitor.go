// Code generated from parser/Ripgen.g4 by ANTLR 4.8. DO NOT EDIT.

package parser // Ripgen

import "github.com/antlr/antlr4/runtime/Go/antlr"

type BaseRipgenVisitor struct {
	*antlr.BaseParseTreeVisitor
}

func (v *BaseRipgenVisitor) VisitStart(ctx *StartContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseRipgenVisitor) VisitTypeIdentifier(ctx *TypeIdentifierContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseRipgenVisitor) VisitPropIdentifier(ctx *PropIdentifierContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseRipgenVisitor) VisitIdentifier(ctx *IdentifierContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseRipgenVisitor) VisitMimeType(ctx *MimeTypeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseRipgenVisitor) VisitRoot(ctx *RootContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseRipgenVisitor) VisitCall(ctx *CallContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseRipgenVisitor) VisitCallName(ctx *CallNameContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseRipgenVisitor) VisitCallBody(ctx *CallBodyContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseRipgenVisitor) VisitCallResponse(ctx *CallResponseContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseRipgenVisitor) VisitCallBinaryResponse(ctx *CallBinaryResponseContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseRipgenVisitor) VisitCallStreamResponse(ctx *CallStreamResponseContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseRipgenVisitor) VisitMethod(ctx *MethodContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseRipgenVisitor) VisitUrlParam(ctx *UrlParamContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseRipgenVisitor) VisitPathPart(ctx *PathPartContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseRipgenVisitor) VisitNamedType(ctx *NamedTypeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseRipgenVisitor) VisitTypeDeclaration(ctx *TypeDeclarationContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseRipgenVisitor) VisitSubTypeSlot(ctx *SubTypeSlotContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseRipgenVisitor) VisitTopTypeSlot(ctx *TopTypeSlotContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseRipgenVisitor) VisitSubTypeDef(ctx *SubTypeDefContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseRipgenVisitor) VisitTopTypeDef(ctx *TopTypeDefContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseRipgenVisitor) VisitIntType(ctx *IntTypeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseRipgenVisitor) VisitFloatType(ctx *FloatTypeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseRipgenVisitor) VisitTimeType(ctx *TimeTypeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseRipgenVisitor) VisitBoolType(ctx *BoolTypeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseRipgenVisitor) VisitStringType(ctx *StringTypeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseRipgenVisitor) VisitAnyType(ctx *AnyTypeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseRipgenVisitor) VisitArrayType(ctx *ArrayTypeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseRipgenVisitor) VisitMapType(ctx *MapTypeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseRipgenVisitor) VisitObjectType(ctx *ObjectTypeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseRipgenVisitor) VisitEnumType(ctx *EnumTypeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseRipgenVisitor) VisitEnumValue(ctx *EnumValueContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseRipgenVisitor) VisitCustomType(ctx *CustomTypeContext) interface{} {
	return v.VisitChildren(ctx)
}
