// Code generated from parser/Ripgen.g4 by ANTLR 4.8. DO NOT EDIT.

package parser // Ripgen

import "github.com/antlr/antlr4/runtime/Go/antlr"

// A complete Visitor for a parse tree produced by RipgenParser.
type RipgenVisitor interface {
	antlr.ParseTreeVisitor

	// Visit a parse tree produced by RipgenParser#start.
	VisitStart(ctx *StartContext) interface{}

	// Visit a parse tree produced by RipgenParser#typeIdentifier.
	VisitTypeIdentifier(ctx *TypeIdentifierContext) interface{}

	// Visit a parse tree produced by RipgenParser#propIdentifier.
	VisitPropIdentifier(ctx *PropIdentifierContext) interface{}

	// Visit a parse tree produced by RipgenParser#identifier.
	VisitIdentifier(ctx *IdentifierContext) interface{}

	// Visit a parse tree produced by RipgenParser#mimeType.
	VisitMimeType(ctx *MimeTypeContext) interface{}

	// Visit a parse tree produced by RipgenParser#root.
	VisitRoot(ctx *RootContext) interface{}

	// Visit a parse tree produced by RipgenParser#call.
	VisitCall(ctx *CallContext) interface{}

	// Visit a parse tree produced by RipgenParser#callName.
	VisitCallName(ctx *CallNameContext) interface{}

	// Visit a parse tree produced by RipgenParser#callBody.
	VisitCallBody(ctx *CallBodyContext) interface{}

	// Visit a parse tree produced by RipgenParser#callResponse.
	VisitCallResponse(ctx *CallResponseContext) interface{}

	// Visit a parse tree produced by RipgenParser#callBinaryResponse.
	VisitCallBinaryResponse(ctx *CallBinaryResponseContext) interface{}

	// Visit a parse tree produced by RipgenParser#callStreamResponse.
	VisitCallStreamResponse(ctx *CallStreamResponseContext) interface{}

	// Visit a parse tree produced by RipgenParser#method.
	VisitMethod(ctx *MethodContext) interface{}

	// Visit a parse tree produced by RipgenParser#urlParam.
	VisitUrlParam(ctx *UrlParamContext) interface{}

	// Visit a parse tree produced by RipgenParser#pathPart.
	VisitPathPart(ctx *PathPartContext) interface{}

	// Visit a parse tree produced by RipgenParser#namedType.
	VisitNamedType(ctx *NamedTypeContext) interface{}

	// Visit a parse tree produced by RipgenParser#typeDeclaration.
	VisitTypeDeclaration(ctx *TypeDeclarationContext) interface{}

	// Visit a parse tree produced by RipgenParser#subTypeSlot.
	VisitSubTypeSlot(ctx *SubTypeSlotContext) interface{}

	// Visit a parse tree produced by RipgenParser#topTypeSlot.
	VisitTopTypeSlot(ctx *TopTypeSlotContext) interface{}

	// Visit a parse tree produced by RipgenParser#subTypeDef.
	VisitSubTypeDef(ctx *SubTypeDefContext) interface{}

	// Visit a parse tree produced by RipgenParser#topTypeDef.
	VisitTopTypeDef(ctx *TopTypeDefContext) interface{}

	// Visit a parse tree produced by RipgenParser#intType.
	VisitIntType(ctx *IntTypeContext) interface{}

	// Visit a parse tree produced by RipgenParser#floatType.
	VisitFloatType(ctx *FloatTypeContext) interface{}

	// Visit a parse tree produced by RipgenParser#timeType.
	VisitTimeType(ctx *TimeTypeContext) interface{}

	// Visit a parse tree produced by RipgenParser#boolType.
	VisitBoolType(ctx *BoolTypeContext) interface{}

	// Visit a parse tree produced by RipgenParser#stringType.
	VisitStringType(ctx *StringTypeContext) interface{}

	// Visit a parse tree produced by RipgenParser#anyType.
	VisitAnyType(ctx *AnyTypeContext) interface{}

	// Visit a parse tree produced by RipgenParser#arrayType.
	VisitArrayType(ctx *ArrayTypeContext) interface{}

	// Visit a parse tree produced by RipgenParser#mapType.
	VisitMapType(ctx *MapTypeContext) interface{}

	// Visit a parse tree produced by RipgenParser#objectType.
	VisitObjectType(ctx *ObjectTypeContext) interface{}

	// Visit a parse tree produced by RipgenParser#enumType.
	VisitEnumType(ctx *EnumTypeContext) interface{}

	// Visit a parse tree produced by RipgenParser#enumValue.
	VisitEnumValue(ctx *EnumValueContext) interface{}

	// Visit a parse tree produced by RipgenParser#customType.
	VisitCustomType(ctx *CustomTypeContext) interface{}
}
