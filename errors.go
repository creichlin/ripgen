package ripgen

import "fmt"

type BadRequestError interface {
	error
	IsBadRequest() bool
}

type badRequest struct {
	cause error
}

func NewBadRequest(err error) error {
	if err == nil {
		return nil
	}
	return &badRequest{cause: err}
}

func (e *badRequest) Error() string {
	return fmt.Sprintf("bad request, %v", e.cause)
}

func (e *badRequest) IsBadRequest() bool {
	return true
}

// Unwrap allows for go1.13+ errors.Unwrap(...), errors.Is(...) and errors.As(...)
func (e *badRequest) Unwrap() error {
	return e.cause
}

// Cause allows for github.com/pkg/errors.Cause(...)
func (e *badRequest) Cause() error {
	return e.cause
}
