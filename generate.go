package ripgen

import (
	"os"
	"path"
	"path/filepath"

	"gitlab.com/creichlin/gopath"
	"gitlab.com/creichlin/ripgen/generate"
	"gitlab.com/creichlin/ripgen/parser"
)

func GenerateGoInterface(source []byte, interfaceFolder string) error {
	ast, err := parser.Parse(string(source))
	if err != nil {
		return err
	}

	err = os.MkdirAll(interfaceFolder, 0700)
	if err != nil {
		return err
	}

	prms := map[string]string{"ipkg": path.Base(interfaceFolder)}

	err = generate.Generate(ast, filepath.Join(interfaceFolder, "interface.gen.go"), "go.interface.gg", prms)
	if err != nil {
		return err
	}
	err = generate.Generate(ast, filepath.Join(interfaceFolder, "dto.gen.go"), "go.dto.gg", prms)
	if err != nil {
		return err
	}
	err = generate.Generate(ast, filepath.Join(interfaceFolder, "enum.gen.go"), "go.enum.gg", prms)
	if err != nil {
		return err
	}

	return nil
}

func GenerateGoServer(source []byte, handlerFolder, interfaceFolder string) error {
	ast, err := parser.Parse(string(source))
	if err != nil {
		return err
	}

	err = os.MkdirAll(handlerFolder, 0700)
	if err != nil {
		return err
	}

	vars, err := addIPKG(interfaceFolder, map[string]string{
		"hpkg": path.Base(handlerFolder),
	})
	if err != nil {
		return err
	}

	err = generate.Generate(ast, filepath.Join(handlerFolder, "handler.gen.go"), "go.handler.gg", vars)
	if err != nil {
		return err
	}

	return nil
}

func GenerateGoProfiler(source []byte, profilerFolder, interfaceFolder string) error {
	ast, err := parser.Parse(string(source))
	if err != nil {
		return err
	}

	err = os.MkdirAll(profilerFolder, 0700)
	if err != nil {
		return err
	}

	vars, err := addIPKG(interfaceFolder, map[string]string{
		"hpkg": path.Base(profilerFolder),
	})
	if err != nil {
		return err
	}

	err = generate.Generate(ast, filepath.Join(profilerFolder, "profiler.gen.go"), "go.profiler.gg", vars)
	if err != nil {
		return err
	}

	return nil
}

func GenerateGoClient(source []byte, clientFolder, interfaceFolder string) error {
	ast, err := parser.Parse(string(source))
	if err != nil {
		return err
	}

	err = os.MkdirAll(clientFolder, 0700)
	if err != nil {
		return err
	}

	vars, err := addIPKG(interfaceFolder, map[string]string{
		"cpkg": path.Base(clientFolder),
	})
	if err != nil {
		return err
	}

	err = generate.Generate(ast, filepath.Join(clientFolder, "client.gen.go"), "go.client.gg", vars)
	if err != nil {
		return err
	}

	return nil
}

func GenerateTypescriptTypes(source []byte, targetFile string) error {
	ast, err := parser.Parse(string(source))
	if err != nil {
		return err
	}

	tsFolder := filepath.Dir(targetFile)

	err = os.MkdirAll(tsFolder, 0700)
	if err != nil {
		return err
	}

	return generate.Generate(ast, targetFile, "ts.types.gg", map[string]string{})
}

func GenerateTypescriptClient(source []byte, targetFile string) error {
	ast, err := parser.Parse(string(source))
	if err != nil {
		return err
	}

	tsFolder := filepath.Dir(targetFile)

	err = os.MkdirAll(tsFolder, 0700)
	if err != nil {
		return err
	}

	return generate.Generate(ast, targetFile, "ts.client.gg", map[string]string{})
}

func addIPKG(pkg string, d map[string]string) (map[string]string, error) {
	d["ipkg"] = filepath.Base(pkg)

	imp, err := gopath.Find(pkg)
	if err != nil {
		return nil, err
	}
	d["ipkgImport"] = imp

	return d, nil
}
